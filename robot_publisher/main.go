package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
	"syscall"
	"time"

	rmq "gitlab.com/lund-university/mean/mean_components/util/rabbitmqutil"
	mess "gitlab.com/lund-university/mean/message"
	event "gitlab.com/lund-university/mean/message/event"
	"gitlab.com/lund-university/mean/robot_publisher/gerrit"
	"golang.org/x/crypto/ssh/terminal"

	"github.com/streadway/amqp"
)

var (
	username                       string
	password                       string
	conn                           *amqp.Connection
	gerritCommentPublisher         rmq.Publisher
	publishedMeanEventPublisher    rmq.Publisher
	notPublishedMeanEventPublisher rmq.Publisher
	publishedNotePublisher         rmq.Publisher
	notPublishedNotePublisher      rmq.Publisher
)

type NoteWithContext struct {
	RequestID     string             `json:"request_id,omitempty"`
	AnalyzerName  string             `json:"analyzer_name,omitempty"`
	Note          *mess.Note         `json:"note,omitempty"`
	SourceContext mess.SourceContext `json:"source_context,omitempty"`
}

type GerritContext struct {
	HostURI     string `json:"host_uri"`
	ProjectName string `json:"project_name"`
	ChangeID    string `json:"change_id"`
	RevisionID  string `json:"revision_id"`
}

type NamedRobotCommentInput struct {
	Name              string
	robotCommentInput []gerrit.RobotCommentInput
}

type RabbitMQReviewInput struct {
	Message       string                   `json:"message,omitempty"`
	Tag           string                   `json:"tag,omitempty"`
	Labels        map[string]int           `json:"labels,omitempty"`
	RobotComments []NamedRobotCommentInput `json:"robot_comments,omitempty"`
}

func NewRabbitMQReviewInput(reviewInput *gerrit.ReviewInput) *RabbitMQReviewInput {
	robotComments := []NamedRobotCommentInput{}
	for key, value := range reviewInput.RobotComments {
		robotComments = append(robotComments, NamedRobotCommentInput{key, value})
	}
	return &RabbitMQReviewInput{
		reviewInput.Message,
		reviewInput.Tag,
		reviewInput.Labels,
		robotComments}
}

func analyzerEventToNotes(ae *mess.AnalyzerEvent) []NoteWithContext {
	notes := []NoteWithContext{}
	if ae.AnalyzerResult == nil {
		return notes
	}
	for _, note := range ae.AnalyzerResult.Notes {
		notes = append(notes, NoteWithContext{
			RequestID:     ae.RequestID,
			AnalyzerName:  ae.AnalyzerName,
			Note:          note,
			SourceContext: ae.SourceContext,
		})
	}
	return notes
}

func getGerritContext(sourceContext map[string]interface{}) GerritContext {
	return GerritContext{
		HostURI:     sourceContext["host_uri"].(string),
		ProjectName: sourceContext["project_name"].(string),
		ChangeID:    sourceContext["change_id"].(string),
		RevisionID:  sourceContext["revision_id"].(string)}
}

func handleRequest(r amqp.Delivery, username, password string) {
	body := r.Body
	var meanEvent mess.MeanEvent

	err := json.Unmarshal(body, &meanEvent)
	if err != nil {
		log.Printf("Error Unmarshalling bytes: %v", err)
	}
	handleMeanEvent(&meanEvent, username, password)
}

func rmqPublish(publisher rmq.Publisher, event interface{}) error {
	eventBytes, err := json.Marshal(event)
	if err != nil {
		return fmt.Errorf("Could not marshal event: %s", err)
	}
	err = publisher.Publish(&amqp.Publishing{
		ContentType: "application/json",
		Body:        eventBytes,
	})
	if err != nil {
		return fmt.Errorf("Could not publish: %s", err)
	}
	return nil
}

func handleMeanEvent(meanEvent *mess.MeanEvent, username, password string) {
	switch meanEvent.EventType {
	case event.AnalyzerEvent:
		switch meanEvent.AnalyzerEvent.EventType {
		case event.Result:
			context := getGerritContext(meanEvent.AnalyzerEvent.SourceContext)
			if !meanEnabled(context.HostURI, context.ProjectName) {
				log.Printf("Mean is disabled for %s. Not publishing.", context.ProjectName)
				for _, noteWCtx := range analyzerEventToNotes(meanEvent.AnalyzerEvent) {
					if err := rmqPublish(notPublishedNotePublisher, noteWCtx); err != nil {
						log.Println(err)
					}
				}
				if err := rmqPublish(notPublishedMeanEventPublisher, meanEvent); err != nil {
					log.Println(err)
				}
				break
			}
			fmt.Println("Create robotcomment for request: " +
				meanEvent.AnalyzerEvent.RequestID +
				" with analyzer: " +
				meanEvent.AnalyzerEvent.AnalyzerName)
			robotComment := createRobotComment(meanEvent.AnalyzerEvent.AnalyzerResult,
				meanEvent.AnalyzerEvent.RequestID,
				meanEvent.AnalyzerEvent.AnalyzerName)
			fmt.Printf("%+v", context)
			body, error := json.Marshal(robotComment)
			if error != nil {
				log.Printf("Error Marshalling struct: %v", error)
			}
			if err := send(body, context, username, password); err != nil {
				log.Println(err)
				break
			}
			robotCommentRabbitMQ := NewRabbitMQReviewInput(robotComment)
			if err := rmqPublish(gerritCommentPublisher, robotCommentRabbitMQ); err != nil {
				log.Println(err)
			}
			if err := rmqPublish(publishedMeanEventPublisher, meanEvent); err != nil {
				log.Println(err)
			}
			for _, noteWCtx := range analyzerEventToNotes(meanEvent.AnalyzerEvent) {
				if err := rmqPublish(publishedNotePublisher, noteWCtx); err != nil {
					log.Println(err)
				}
			}
		default:
			fmt.Println(meanEvent.AnalyzerEvent.EventType.String() + " is not implemented for AnalyzerEvent")
		}
	case event.AnalyzeRequest:
		fmt.Println("Received a AnalyzerRequest")
	default:
		fmt.Println("Incorrect eventtype for robotpublisher")
	}
}

func createRobotComment(analyzerResult *mess.AnalyzerResult, requestID, analyzerName string) *gerrit.ReviewInput {
	message := fmt.Sprintf("%s found %v potential issues", analyzerName, len(analyzerResult.Notes))
	reviewValue := 0

	labels := map[string]int{"Code-Review": reviewValue}

	robotComments := make(map[string][]gerrit.RobotCommentInput)

	for _, note := range analyzerResult.Notes {
		rci := gerrit.RobotCommentInput{}
		currentTime := time.Now().Format("2006-01-02 15:04:05.000000000")
		aRange := note.Location.Range
		if aRange.StartLine != 0 {
			var sl, sc, el, ec uint32
			sl = aRange.StartLine
			el = aRange.EndLine
			sc = 0
			ec = 0
			/*if aRange.StartColumn != 0 {
				sc = aRange.StartColumn - 1
			}
			if aRange.EndColumn != 0 {
				ec = aRange.EndColumn - 1
			}*/
			if el == 0 {
				el = sl
				ec = 0
			}
			if sc != 0 || ec != 0 {
				el++
			}
			rci.Range = &gerrit.CommentRange{StartLine: sl, StartCharacter: sc, EndLine: el, EndCharacter: ec}
		}
		url := note.Url
		rci.Message = fmt.Sprintf("%s\n\nCategory: %s/%s\n%s", note.Description, strings.Title(analyzerName), note.Category, url)
		fmt.Println(note.Description)
		rci.Updated = currentTime
		rci.RobotId = analyzerName
		rci.RobotRunId = requestID
		rci.Unresolved = true
		path := note.Location.Path
		rci.FixSuggestions = getSuggestions(note)
		rci.Properties = map[string]interface{}{"note_id": note.NoteId, "analyzer_name": analyzerName, "category": note.Category}
		robotComments[path] = append(robotComments[path], rci)
	}

	return &gerrit.ReviewInput{Message: message, Labels: labels, RobotComments: robotComments}
}

func getSuggestions(note *mess.Note) [] gerrit.FixSuggestionInfo {
	var gerritFixSuggestions [] gerrit.FixSuggestionInfo
	for _, nfsi := range note.Replacements {
		var fsi gerrit.FixSuggestionInfo
		var gerritFixReplacements [] gerrit.FixReplacementInfo
		for _, nfri := range nfsi.Replacements {
			var fri gerrit.FixReplacementInfo
			fri.Path = nfri.Location.Path
			aRange := nfri.Location.Range
			if aRange.StartLine != 0 {
				fri.Range = getRange(aRange)
			}
			fri.Replacement = nfri.Replacement
			gerritFixReplacements = append(gerritFixReplacements, fri)
		}
		fsi.Replacements = gerritFixReplacements
		fsi.Description = nfsi.Description
		gerritFixSuggestions = append(gerritFixSuggestions, fsi)
	}
	return gerritFixSuggestions
}

func getRange(aRange *mess.TextRange) *gerrit.CommentRange {
	var sl, sc, el, ec uint32
	sl = aRange.StartLine
	el = aRange.EndLine

	sc = aRange.StartColumn
	ec = aRange.EndColumn
	if el == 0 {
		el = sl
		ec = 0
	}
	return &gerrit.CommentRange{StartLine: sl, StartCharacter: sc, EndLine: el, EndCharacter: ec}
}

func send(data []byte, context GerritContext, username, password string) error {
	fmt.Printf("\n ProjectName: %s \n", context.ProjectName)
	fmt.Println(string(data))
	hostURI := context.HostURI

	//Url should be updated in future, this method will be deprecated in the future according to gerrit docs
	changeID := context.ChangeID
	revisionID := context.RevisionID
	url2 := hostURI + "/a/changes/" + changeID + "/revisions/" + revisionID + "/review"
	fmt.Println(url2)
	req, error := http.NewRequest("POST", url2, bytes.NewReader(data))
	if error != nil {
		return fmt.Errorf("Error creating Request: %v", error)
	}
	req.SetBasicAuth(username, password)
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, error := client.Do(req)
	if error != nil {
		return fmt.Errorf("Error sending POST: %v", error)
	}
	body, _ := ioutil.ReadAll(resp.Body)

	fmt.Println("Gerrit says: ")
	fmt.Println(string(body))
	return nil
}

func meanEnabled(hostURI, project string) bool {
	url := hostURI + "/a/projects/" + url.PathEscape(project) + "/meanenabled"
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Printf("Error creating Request: %v", err)
		return false
	}
	req.SetBasicAuth(username, password)
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Printf("Could not ask gerrit about meanhost: %v", err)
		return false
	}
	if resp.StatusCode != 200 {
		log.Printf("Project %s: %s", project, resp.Status)
		return false

	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Could not read response body: %v", err)
		return false
	}
	trimmed := bytes.TrimLeft(body, `)]}'`)
	var meanEnabled struct {
		Enable bool `json:"enable,omitempty"`
	}
	err = json.Unmarshal(trimmed, &meanEnabled)
	if err != nil {
		log.Printf("Could not unmarshal json: %v", err)
		return false
	}
	return meanEnabled.Enable
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func failIfErr(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func main() {
	fmt.Println("Start Gerrit Robot-publisher Service")

	username = /* replace */
	fmt.Printf("Password for \"%s\": ", username)
	pwd, err := terminal.ReadPassword(syscall.Stdin)
	password = string(pwd)
	fmt.Println()
	failIfErr(err)
	const HOST = /* replace */
	conn, err = amqp.Dial(
		HOST,
	)
	failIfErr(err)
	defer conn.Close()

	const EXCHANGE = /* replace */
	gerritCommentPublisher = rmq.NewSimplePublisher(conn, EXCHANGE, "published-gerrit-comments")
	publishedMeanEventPublisher = rmq.NewSimplePublisher(conn, EXCHANGE, "published-mean-events")
	notPublishedMeanEventPublisher = rmq.NewSimplePublisher(conn, EXCHANGE, "not-published-mean-events")
	publishedNotePublisher = rmq.NewSimplePublisher(conn, EXCHANGE, "published-notes")
	notPublishedNotePublisher = rmq.NewSimplePublisher(conn, EXCHANGE, "not-published-notes")

	consumer := rmq.NewSimpleConsumer(conn, "svcmean.mean.to-gerrit")

	for {
		msgs, err := consumer.Consume()
		if err != nil {
			fmt.Printf("Could not consume: %v", err)
			fmt.Println("Trying again ...")
			time.Sleep(time.Duration(time.Second))
			continue
		}
		log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
		for d := range msgs {
			go handleRequest(d, username, password)
		}
		log.Printf("Channel unexpectedly closed")
	}
}
