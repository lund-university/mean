package gerrit

type ReviewInput struct {
	Message string         `json:"message,omitempty"`
	Tag     string         `json:"tag,omitempty"`
	Labels  map[string]int `json:"labels,omitempty"`
	//Comments              `json:"comments,omitempty"`
	RobotComments map[string][]RobotCommentInput `json:"robot_comments,omitempty"`
	//Drafts                `json:"drafts,omitempty"`
	//Notify                `json:"notify,omitempty"`
	//NotifyDetails         `json:"notify_details,omitempty"`
	//OmitDuplicateComments `json:"omit_duplicate_comments,omitempty"`
	//OnBehalfOf            `json:"on_behalf_of,omitempty"`
	//Reviewers             `json:"reviewers,omitempty"`
	//Ready                 `json:"ready,omitempty"`
	//WorkInProgress        `json:"work_in_progress,omitempty"`
}

type RobotCommentInput struct {
	//PatchSet       `json:"patch_set,omitempty"`
	Id string `json:"id"`
	//Path           `json:"path,omitempty"`
	//Side           `json:"side,omitempty"`
	//Parent         `json:"parent,omitempty"`
	//Line           `json:"line,omitempty"`
	Range *CommentRange `json:"range,omitempty"`
	//InReplyTo      `json:"in_reply_to,omitempty"`
	Message string `json:"message,omitempty"`
	Updated string `json:"updated"`
	//Author         `json:"author,omitempty"`
	//Tag            `json:"tag,omitempty"`
	Unresolved     bool                   `json:"unresolved,omitempty"`
	RobotId        string                 `json:"robot_id"`
	RobotRunId     string                 `json:"robot_run_id"`
	Url            string                 `json:"url,omitempty"`
	Properties     map[string]interface{} `json:"properties,omitempty"`
	FixSuggestions []FixSuggestionInfo    `json:"fix_suggestions,omitempty"`
}

//TODO: what types should the fields in CommentRange be.
type CommentRange struct {
	StartLine      uint32 `json:"start_line"`
	StartCharacter uint32 `json:"start_character"`
	EndLine        uint32 `json:"end_line"`
	EndCharacter   uint32 `json:"end_character"`
}

type FixSuggestionInfo struct {
	//FixId        `json:"fix_id,omitempty"`
	Description string `json:"description"`
	Replacements []FixReplacementInfo `json:"replacements"`
}

type FixReplacementInfo struct {
	Path string `json:"path"`
	Range *CommentRange `json:"range"`
	Replacement string  `json:"replacement"`
}
