package event

import (
	"encoding/json"
	"fmt"
)

type EventType interface {
	toString() (string, bool)
	toEvent(string) (EventType, bool)
}

type AnalyzerEventType uint8
type MeanEventType uint8

const (
	Error AnalyzerEventType = iota + 1
	Started
	Result
	Timeout
	Scheduled
	NotRelevant
)
const (
	AnalyzerEvent MeanEventType = iota + 1
	AnalyzeRequest
	MeanRequestError
)

var toStringMean = map[EventType]string{
	AnalyzerEvent:    "analyzer_event",
	AnalyzeRequest:   "analyze_request",
	MeanRequestError: "mean_request_error",
}

var toStringAnalyzer = map[EventType]string{
	Error:       "error",
	Started:     "started",
	Result:      "result",
	Timeout:     "timeout",
	Scheduled:   "scheduled",
	NotRelevant: "not_relevant",
}

func (s MeanEventType) toString() (string, bool) {
	str, ok := toStringMean[s]
	return str, ok
}

func (s MeanEventType) String() string {
	return toStringMean[s]
}

func (s AnalyzerEventType) toString() (string, bool) {
	str, ok := toStringAnalyzer[s]
	return str, ok
}

func (s AnalyzerEventType) String() string {
	return toStringAnalyzer[s]
}

func (MeanEventType MeanEventType) toEvent(str string) (EventType, bool) {
	result, ok := toMeanEvent[str]
	return result, ok
}

func (AnalyzerEventType AnalyzerEventType) toEvent(str string) (EventType, bool) {
	result, ok := toAnalyzerEvent[str]
	return result, ok
}

var toMeanEvent = inverse(toStringMean)
var toAnalyzerEvent = inverse(toStringAnalyzer)

func inverse(m map[EventType]string) map[string]EventType {
	n := make(map[string]EventType)
	for k, v := range m {
		n[v] = k
	}
	return n
}

// MarshalJSON marshals the enum as a quoted json string
func (s MeanEventType) MarshalJSON() ([]byte, error) {
	return marshalJSON(&s)
}

func (s AnalyzerEventType) MarshalJSON() ([]byte, error) {
	return marshalJSON(&s)
}

func marshalJSON(s EventType) ([]byte, error) {
	str, ok := s.toString()
	if !ok {
		return nil, fmt.Errorf("Can not marshal EventType: %d", s)
	}
	return []byte(`"` + str + `"`), nil
}

// UnmarshalJSON unmashals a quoted json string to the enum value
func (s *MeanEventType) UnmarshalJSON(b []byte) error {
	eventType, err := unmarshalJSON(s, b)
	if err != nil {
		return err
	}
	*s = eventType.(MeanEventType)
	return nil
}

func (s *AnalyzerEventType) UnmarshalJSON(b []byte) error {
	eventType, err := unmarshalJSON(s, b)
	if err != nil {
		return err
	}
	*s = eventType.(AnalyzerEventType)
	return nil
}

func unmarshalJSON(s EventType, b []byte) (EventType, error) {
	var str string
	err := json.Unmarshal(b, &str)
	if err != nil {
		return nil, err
	}
	result, ok := s.toEvent(str)
	if !ok {
		return result, fmt.Errorf("Can not unmarshal \"%s\" to EventType", str)
	}
	return result, nil
}
