package event

import (
	"encoding/json"
	"testing"
)

type EventStruct struct {
	Str string            `json:"str"`
	A   AnalyzerEventType `json:"a"`
}

var (
	testJSON   = `{"str":"good","a":"result"}`
	testStruct = EventStruct{
		Str: "good",
		A:   Result,
	}

	testJSONBad   = `{"str":"bad","a":"non valid event"}`
	testStructBad = EventStruct{
		Str: "bad",
		A:   99,
	}
)

func TestUnmarshalValidJSON(t *testing.T) {
	var a AnalyzerEventType
	err := a.UnmarshalJSON([]byte(`"result"`))
	if err != nil {
		t.Error(err)
	}
	if a != Result {
		t.Errorf("Failed to unmarshal result")
	}
}

func TestUnmarshalValidMeanEventTypeJSON(t *testing.T) {
	var a MeanEventType
	err := a.UnmarshalJSON([]byte(`"analyze_request"`))
	if err != nil {
		t.Error(err)
	}
	if a != AnalyzeRequest {
		t.Errorf("Failed to unmarshal result")
	}
}

func TestUnmarshalInvalidJSON(t *testing.T) {
	var a AnalyzerEventType
	err := a.UnmarshalJSON([]byte(`"not valid analyzerevent"`))
	if err == nil {
		t.Error("Expected unmarshal error")
	}
}

func TestUnmarshalInvalidMeanEventTypeJSON(t *testing.T) {
	var a MeanEventType
	err := a.UnmarshalJSON([]byte(`"not valid meanevent"`))
	if err == nil {
		t.Error("Expected unmarshal error")
	}
}

func TestMarshalValidEvent(t *testing.T) {
	bytes, err := Started.MarshalJSON()
	if err != nil {
		t.Error(err)
	}
	if string(bytes) != `"started"` {
		t.Errorf("Expected \"started\". Got %s.", string(bytes))
	}
}

func TestMarshalValidMeanEventTypeEvent(t *testing.T) {
	bytes, err := AnalyzerEvent.MarshalJSON()
	if err != nil {
		t.Error(err)
	}
	if string(bytes) != `"analyzer_event"` {
		t.Errorf("Expected \"started\". Got %s.", string(bytes))
	}
}

func TestMarshalInvalidEvent(t *testing.T) {
	var event AnalyzerEventType = 10
	_, err := event.MarshalJSON()
	if err == nil {
		t.Error("Expected marshal error")
	}
}

func TestMarshalStruct(t *testing.T) {
	bytes, err := json.Marshal(testStruct)
	if err != nil {
		t.Error(err)
	}
	if string(bytes) != testJSON {
		t.Errorf("Expected %s. Got %s", testJSON, string(bytes))
	}
}

func TestMarshalBadStruct(t *testing.T) {
	_, err := json.Marshal(testStructBad)
	if err == nil {
		t.Error("Expected marshal error ")
	}
}

func TestUnmarshalStruct(t *testing.T) {
	var event EventStruct
	err := json.Unmarshal([]byte(testJSON), &event)
	if err != nil {
		t.Error(err)
	}
	if event != testStruct {
		t.Errorf("Expected %+v. Got %+v", testStruct, event)
	}
}
