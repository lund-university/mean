package message

import "gitlab.com/lund-university/mean/config"

type (
	MeanRequest struct {
		RequestID     string             `json:"request_id"`
		Paths         []string           `json:"paths"`
		SourceContext SourceContext      `json:"source_context"`
		Config        config.LocalConfig `json:"config,omitempty"`
	}
)
