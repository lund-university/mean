package message

type (
	AnalyzeRequest struct {
		// RequestID is the ID of the mean request
		RequestID             string        `json:"request_id"`
		AnalyzerName          string        `json:"analyzer_name"`
		DockerImage           string        `json:"docker_image"`
		Paths                 []string      `json:"paths"`
		SourceContext         SourceContext `json:"source_context"`
		Timeout               uint32        `json:"timeout,omitempty"`
		BlacklistedCategories []string      `json:"blacklisted_categories,omitempty"`
	}
)
