package message

type (
	Location struct {
		Path string `json:"path"`
		// If nil, analyze entire file
		Range *TextRange `json:"range,omitempty"`
	}

	TextRange struct {
		// If 0 or empty, analyze entire file and ignore fields below
		StartLine uint32 `json:"start_line,omitempty"`
		// If 0 or empty, position range is a span of lines and the field EndColumn is ignored.
		// If set, StartLine must also be set.
		StartColumn uint32 `json:"start_column,omitempty"`
		// If 0 or empty, then defaults to the same value as start_line. This line is included
		// in the range, i.e. the range spans (EndLine-StartLine+1) lines.
		// If set, StartLine must also be set
		EndLine uint32 `json:"end_line,omitempty"`
		// If 0 or empty, then the position spans until the end of end_line. This column is
		// included in the range, i.e. the range spans (EndColumn-StartColumn+1) // columns.
		// If set, all other fields must be set as well.
		EndColumn uint32 `json:"end_column,omitempty"`
	}
)
