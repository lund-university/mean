package message

import event "gitlab.com/lund-university/mean/message/event"

type (
	// AnalyzerResult is what the analyzer dockerimages give as output
	AnalyzerResult struct {
		Notes  []*Note  `json:"notes"`
		Errors []string `json:"errors"`
	}

	// TODO: should this be added
	//ErrorInfo struct {
	//	ErrorCode int
	//	Description string
	//}

	// AnalyzerEvent is sent from analyzers and analyzer analyzer executor to mean.
	// They are all bound to a single execution of an analyzer by (RequestID, Analyzername).
	AnalyzerEvent struct {
		// RequestID is the ID of the mean request
		RequestID    string `json:"request_id"`
		AnalyzerName string `json:"analyzer_name"`

		EventType      event.AnalyzerEventType `json:"event_type"`
		AnalyzerResult *AnalyzerResult         `json:"analyzer_result,omitempty"`
		SourceContext  SourceContext           `json:"source_context,omitempty"`
		Info           string                  `json:"info,omitempty"`
	}

	Note struct {
		NoteId      string   `json:"note_id"`
		Location    Location `json:"location"`
		Description string   `json:"description"`
		Category    string   `json:"category"`
		Url         string   `json:"url,omitempty"`
		Replacements []FixSuggestionInfo  `json:"replacements,omitempty"`
	}

	FixSuggestionInfo struct {
		//FixId        `json:"fix_id,omitempty"`
		Description string `json:"description"`
		Replacements []FixReplacementInfo `json:"replacements"`
	}

	FixReplacementInfo struct {
		Location    Location `json:"location"`
		Replacement string  `json:"replacement"`
	}
)
