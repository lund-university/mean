package message

import event "gitlab.com/lund-university/mean/message/event"

type (
	MeanEvent struct {
		EventType      event.MeanEventType `json:"event_type"`
		AnalyzerEvent  *AnalyzerEvent      `json:"analyzer_event,omitempty"`
		AnalyzeRequest *AnalyzeRequest     `json:"analyze_request,omitempty"`
		MeanRequest    *MeanRequest        `json:"mean_request,omitempty"`
		Info           string              `json:"info,omitempty"`
	}
)
