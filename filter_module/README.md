# Filter Module
This module is responsible for filtering out analysis results that are not related to the most recent changed lines. This is done by performing a git diff on the analyzed repository after an analyzer is finished running.

# How to run
Make a small addition to the Analyzer Executor Jenkins file at the end of the "run analyzer" stage to start the filter module.
