from subprocess import Popen, PIPE
import json
import re

RESULT_FILENAME = "/mean/output/result.json"

def find_changed_lines(lines):
  changed_lines = dict()
  
  filename = ""
  for line in lines:
    match = re.search("diff --git a/[^\s]* b/([^\s]*)", line) # Get filename for diff
    if match:
      filename = match.group(1)
      changed_lines[filename] = set()
    else:
      match = re.search("@@ -[^\s]* \+([^\s]*) @@", line) # Get unified diff
      if match:
        change = match.group(1)
        start = 1
        count = 1
        if "," in change: # Calculate start and count for diff
          split = change.split(",")
          start = int(split[0])
          count = int(split[1])
        else:
          start = int(change);

        for i in range(start, start + count):
          changed_lines[filename].add(i)
  return changed_lines

def get_analysis_result(filename):
  with open(filename, "r") as resultfile:
    result = json.load(resultfile)
  return result;

def set_analysis_results(result, filename):
  with open(filename, "w+") as resultfile:
    json.dump(result, resultfile)


def filter_result(changed_lines, result):
  valid_notes = []
  valid = False
  for note in result["notes"]:
    valid = False
    location = note["location"]
    if location["path"] in changed_lines:
      change = changed_lines[location["path"]]
      rnge = location["range"]
      if "start_line" not in rnge or rnge["start_line"] == 0:
        valid = True
      else:
        start_line = rnge["start_line"]
        end_line = start_line
        if "end_line" in rnge:
          end_line = rnge["end_line"]
        for line in range(start_line, end_line+1):
          if line in change:
            valid = True
            break
    elif location["path"] == "/COMMIT_MSG":
      valid = True
    if valid:
      valid_notes.append(note)
    else:
      print("Filtered away: " + str(note))
  result["notes"] = valid_notes
      

def main():
  proc = Popen(["git", "-C", "/mean/code", "diff", "--unified=0", "@~..@"], stdout = PIPE, stderr = PIPE)
  stdout, stderr = proc.communicate()
  print(stderr.decode("utf-8", "ignore"))
  changed_lines = find_changed_lines(stdout.decode("utf-8", "ignore").split("\n"))
  try:
    result = get_analysis_result(RESULT_FILENAME)
    filter_result(changed_lines, result)
    set_analysis_results(result, RESULT_FILENAME)
  except IOError:
    print("No result file, nothing to filter.")

if __name__ == '__main__':
  main()
