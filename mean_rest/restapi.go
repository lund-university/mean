package mean

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"

	meanpkg "gitlab.com/lund-university/mean/mean"
)

var mean *meanpkg.Mean

func progress(res http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)

	var statuses interface{}

	if reqID, ok := vars["reqID"]; ok {
		statuses = mean.Statuses(reqID)
	} else {
		statuses = mean.AllStatuses()
	}

	js, err := json.Marshal(statuses)

	if err != nil {
		log.Print(err)
		http.Error(res, err.Error(), http.StatusInternalServerError)
		return
	}

	res.Header().Set("Content-Type", "application/json")
	res.Write(js)
}

// StartProgressREST starts a rest api which returns state of analyzer progress
func StartProgressREST(m *meanpkg.Mean) {
	mean = m
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/progress/{reqID}", progress)
	router.HandleFunc("/progress", progress)
	log.Fatal(http.ListenAndServe(":10000", router))
}
