# Local MEAN setup

This guide goes through the different steps needed to get MEAN up and running on a local setup. This includes setting up Gerrit, Jenkins, RabbitMQ and other systems. Note: This document is a WIP and there could be errors and/or parts of the guide could be hard to follow. With that said, time to get started: 

### Install Docker:
  - Install Docker (for example via apt-get). Docker is used to encapsulate several different MEAN components (for example the analyzers).
  - To avoid having to run all Docker commands with sudo, you should add yourself (and later the jenkins user as well) to the correct groups, as detailed in [this tutorial](https://linuxhandbook.com/docker-permission-denied/).

### Setup MEAN:
  - Create folder named 'go' in home
  - execute command 'git clone https://gitlab.com/lund-university/mean ~/go/src/gitlab.com/lund-university/mean'
  - Get the necessary dependencies for MEAN:
    * go get -v github.com/syndtr/goleveldb/leveldb
    * go get -v golang.org/x/crypto/ssh/terminal
    * go get -v github.com/streadway/amqp
    * go get -v github.com/gorilla/mux
    * go get -v gopkg.in/yaml.v3
    * go get -v go.mongodb.org/mongo-driver/mongo
  - Modify MEAN source files in the repository you just cloned, to update HOST, EXCHANGE, PORT, variables and other things:
    * analyzer\_trigger/mean\_publisher.py:
      + URL (url to gerrit): 'http://localhost:8081'
      + HOST (url to rabbitmq): 'localhost'
      + PORT (port to rabbitmq): 5672
      + EXCHANGE: 'meanexchange'
      + Change the if condition on line 109 to false ('if False:'), because we do not want ssl when running MEAN locally, that is just a hassle with certificates etc.
    * cmd/jenkinsanalyze/jenkinsanalyze.go:
      + line 25: set rmqPort to "5672"
      + line 26: set EXCHANGE to "meanexchange"
      + line 40: remove the 's' in 'amqps' (again, to disable ssl)
    * robot\_publisher/main.go:
      + line 343: set username to "svcmean"
      + line 349: set HOST to "amqp://guest:guest@localhost:5672/"
      + line 356: set EXCHANGE to "meanexchange"
    * analyzer\_executor/Jenkinsfile:
      + Too many changes need to be made in this file to be detailed here. Instead, a modified Jenkinsfile is included at the end of this file.
    * not-useful-server/not-useful-server.py:
      + PORT (port to rabbitmq): 5672
      + EXCHANGE: 'meanexchange'
      + ROUTING\_KEY: 'not-useful'
      + Replace the else-statement in the main method with:
        ```
        else:
          parameters = pika.ConnectionParameters(host = 'localhost', port = 5672)
        ```
      + Change the 'app.run()' statement to:
        ```
        app.run(host='localhost',debug=False)
        ```
  - Go to the MEAN git repo and run command: 'go install ./...'
  - Build docker images in following directories in the MEAN project:
    * analyzer\_executor/
      + No need to run the docker\_build.sh since artifactory is not used in this guide. 
      + Run: "$ docker build -t rabbitmq\_publisher -f Dockerfile ."
    * analyzer\_trigger/
      + Comment out or remove the two lines under build: starting with "@<!-- -->docker tag"
      + Run: "$ make"
    * analyzers/\*\_analyzer/
      + If Makefile exists then comment out or remove the two lines under build: starting with "@<!-- -->docker tag" and run with "$ make"
      + If docker\_build.sh exists, run "docker\_build.sh". Since the build is local it is also possible to extract "@<!-- -->docker build --tag=\<img\> --file=\<dockerfile\> \<context\>" from the shell script.
      + Some folders contain both a Makefile and a "docker\_build.sh" file, in these cases either one of the two above steps can be chosen.
    * Debugging:
      + Problems with building docker images with errors from pip, apt-get, etc. are most likely due to company proxy configurations.
      + Some docker images 
    * After the images are built, you can see a list of them by running the command 'docker images' will not be tagged with ":latest", for example "mean\_pylint". This may cause problems later down the line since, if not specified, only ":latest" tagged images will be searched for. To alleviate this problem one can add to the Makefile under build: "@<!-- -->docker build -t $\{NAME\} -f $\{DOCKERFILE\} $\{CONTEXT\}".

### Setup Gerrit:
  - Follow [this tutorial](https://gerrit-review.googlesource.com/Documentation/linux-quickstart.html) to get Gerrit up and running
    * Note: Replace every occurrence of the port number (8080) with 8081, since we want Jenkins to use 8080.
  - If Gerrit complains about lock when starting: delete *.lock files in folders in gerrit\_testsite/index and kill Gerrit process (find Gerrit process id with command 'ps aux \| grep gerrit')
  - View Gerrit: open web browser and surf to 'localhost:8081'. If unable to when running on this with a LTS, check /etc/hosts.
  - Create new Gerrit project: ssh -p 29418 admin@localhost gerrit create-project testproject.git
  - Push to Gerrit:
    * Create new git repo (with command 'git init')
    * Create new file in repo (for example 'touch README.md')
    * Commit: 'git commit -m "Some commit message"'
    * Push to Gerrit: git push ssh://admin@localhost:29418/testproject HEAD:refs/for/master
    * If git complains about missing Change-Id: Follow the hint in the terminal to solve the issue
    * Go to Gerrit in browser to make sure that the change was pushed to the Gerrit project
  - Create a new Gerrit user called 'jenkins', which is the user that Jenkins will use to interact with Gerrit (will be used for example when configuring the Gerrit Trigger Plugin)
    * Generate a HTTP password in Gerrit for the jenkins user. This is done in Settings -> HTTP Credentials -> GENERATE NEW PASSWORD
    * Note 1: Save this password, because it can only be viewed once (and if you generate a new password the old will become invalid and needs to be changed)
    * Note 2: If your generated password has weird characters (such as '/') you are going to have a bad time (although '+' seems to be ok). I would advise generating a new password immediately to avoid later hassle. This can also be solved by replacing the special characters with their unicode representations, for instance, replace '/' with '%2F'.
    * NOTE 3: Every time the all-caps word 'PASSWORD' appears in this guide, it refers to this Gerrit password.

### Setup Jenkins:
  - Tutorial:
    * https://www.jenkins.io/download/
    * https://www.jenkins.io/doc/book/installing/linux/
  - Note: Be sure to install default plugins when asked
  - View Jenkins: surf to 'localhost:8080' in web browser
  - If proxy is used: Make sure to enter the correct configuration, so that Jenkins can download plugins
  - How to restart Jenkins (if needed at any time): sudo service jenkins [stop/start/restart]

### Install Jenkins plugin Gerrit Trigger:
  - Follow [this tutorial](https://plugins.jenkins.io/gerrit-trigger/)
    * Follow until (not including) 'Dynamic triggering', while at the same time creating Jenkins item:
      + To add "Label Verified:" to gerrit, since this was removed several versions ago, please follow this [guide](https://blog.bruin.sg/2013/04/19/how-to-edit-the-project-config-for-all-projects-in-gerrit/).
      + In the Administrative Settings part of the tutorial:
        - if it complains that the ssh-key "is not a valid key file". 
  Then creating a new key with the command "ssh-keygen -m PEM" should work. 
        - It might also complain that the key is missing. 
  To fix this you'll need to change the owner of the ssh keypair to jenkins by writing 
  'sudo chown jenkins:jenkins key_name*' in the .ssh directory.
      + The tutorial is old and therefore inaccurate in some parts. When prompted to restart the connection in the "Control" section, such a section might not be there. Instead, there should be a red icon under "status", click on it and it should turn blue, and the connection should have been started correctly.
    * Create a new Jenkins item: Pipeline, and specify gerrit trigger
      + Under 'Build Triggers', specify 'Gerrit event'
      + Under 'Gerrit Trigger':
        - Choose Trigger on -> Patchset Created
        - Choose Type: 'Plain' and Pattern: 'testproject' under 'Gerrit Project'
        - Choose Type: 'Path' and Pattern: '**' under 'Branches'
      + Select 'Hello World' as Pipeline script (This will be changed in a later step, but hello world is fine for now as testing)
    * Push new change to Gerrit and check that the Jenkins job is triggered

### Install and configure RabbitMQ:
  - Start RabbitMQ in docker container: docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3-management
  - Surf to localhost:15672 in web browser, login as user 'guest' (password 'guest')
  - Run below script to configure exchanges, routing keys, and queues: TODO: This script is not 100 % complete yet
      ```
      wget http://localhost:15672/cli/rabbitmqadmin
      chmod +x rabbitmqadmin
      ./rabbitmqadmin declare exchange name=meanexchange type=direct
      ./rabbitmqadmin declare queue name=svcmean.mean.analyzer-events
      ./rabbitmqadmin declare queue name=svcmean.mean.analyzer-requests
      ./rabbitmqadmin declare queue name=svcmean.mean.mean-requests
      ./rabbitmqadmin declare queue name=svcmean.mean.log-events
      ./rabbitmqadmin declare queue name=svcmean.mean.to-gerrit
      ./rabbitmqadmin declare queue name=svcmean.mean.storage
      ./rabbitmqadmin declare binding source=meanexchange destination=svcmean.mean.mean-requests routing_key=mean-requests
      ./rabbitmqadmin declare binding source=meanexchange destination=svcmean.mean.analyzer-requests routing_key=analyze-requests
      ./rabbitmqadmin declare binding source=meanexchange destination=svcmean.mean.to-gerrit routing_key=mean-events
      ./rabbitmqadmin declare binding source=meanexchange destination=svcmean.mean.analyzer-events routing_key=analyzer-events
      ./rabbitmqadmin declare binding source=meanexchange destination=svcmean.mean.storage routing_key=published-gerrit-comments
      ./rabbitmqadmin declare binding source=meanexchange destination=svcmean.mean.storage routing_key=published-mean-events
      ./rabbitmqadmin declare binding source=meanexchange destination=svcmean.mean.storage routing_key=not-published-mean-events
      ./rabbitmqadmin declare binding source=meanexchange destination=svcmean.mean.storage routing_key=published-notes
      ./rabbitmqadmin declare binding source=meanexchange destination=svcmean.mean.storage routing_key=not-published-notes
      ./rabbitmqadmin declare binding source=meanexchange destination=svcmean.mean.storage routing_key=not-useful
      ```

### Start MEAN:
  - Go to ~/go/bin
  - Start main MEAN service: 'sudo ./jenkinsanalyze -config ../src/gitlab.com/lund-university/mean/config/exampleconfigs/global-config-example.yml  -rmqhost localhost -rmquser guest'
  - Enter 'guest' when prompted for 'Password for "guest"'
  - Start the Robot Publisher:
    - Create a new user called 'svcmean' in Gerrit, and generate a HTTP password for it
    - Run: 'sudo ./robot\_publisher'
    - Enter the Gerrit HTTP password you just generated for the svcmean user (The reason for this is because the Gerrit MEAN plugin only enabled the 'not-useful' button for robot comments sent by the specific user 'svcmean')
  - Start Not Useful Server:
    * Go to the MEAN git repository
    * Run: 'python not-useful-server/not-useful-server.py'

### Install mean-gerrit-plugin in Gerrit:
  - This step is a bit arduous (and takes a while to complete), since the plugin needs to be re-built, which in turn requires the complete Gerrit Tree.
  - Install the build tool Bazel: [Tutorial](https://docs.bazel.build/versions/4.0.0/install-ubuntu.html)
  - Follow this [tutorial](https://gerrit-review.googlesource.com/Documentation/dev-bazel.html) or [this](http://www.scmgalaxy.com/tutorials/how-to-compile-and-build-gerrit-plugins/) tutorial to create a Gerrit Tree
  - Clone the mean plugin into the Gerrit tree ([gitlab link](https://gitlab.com/lund-university/mean-gerrit-plugin))
  - Important: Open the BUILD file in mean-gerrit-plugin and change the line 'name = "mean",' to 'name = "mean-gerrit-plugin",'
  - Run command: 'bazel build plugins/mean-gerrit-plugin'
  - Locate 'bazel-bin/plugins/mean-gerrit-plugin/mean-gerrit-plugin.jar' and move it to your Gerrit plugin folder ('~/gerrit\_testsite/plugins/')
  - Configure the MEAN Gerrit plugin:
    * Create new git repository
    * Pull the Gerrit config: git pull ssh://admin@localhost:29418/testproject refs/meta/config
    * Create file 'mean.config' and add the below configuration to it:
        ```
        [general]
            enable = true
        [analyzer "shellcheck"]
            enable = true
            timeout = 60
        [analyzer "pylint"]
            enable = true
            timeout = 60
        [analyzer "hadolint"]
            enable = true
            timeout = 60
        [analyzer "ansible-lint"]
            enable = true
            timeout = 60
        [analyzer "flake8"]
            enable = true
            timeout = 60
        ```
    * Commit change
    * Push: 'git push ssh://admin@localhost:29418/testproject HEAD:refs/meta/config'
  - Global configuration:
    + Create another file called 'mean.config', with the following content:
      ```
      [host]
        url = http://localhost:5000/
      ```
    + Place the file in '~/gerrit\_testsite/etc/'
  - Restart Gerrit to make sure that the changes take effect
  - Surf to 'http://localhost:8081/a/projects/testproject/meanenabled' and check that the json returned contains '"enable": true'

### Setup analyzer\_trigger (MEAN publisher):
  - Replace the hello world script from [this step](#install-jenkins-plugin-gerrit-trigger) with this script:
      ```
      node {
          stage('docker') {
              sh "docker run --network=\"host\" -e GERRIT_PROJECT -e GERRIT_CHANGE_ID -e GERRIT_PATCHSET_REVISION -e GERRIT_REFSPEC -e GERRIT_CHANGE_NUMBER mean_publisher:9b44b76 jenkins PASSWORD"
          }
      }
      ```
  - Replace "PASSWORD" in the script with the Gerrit PASSWORD
  - Test run it with the 'Build Now' button in Jenkins (the script will always fail, but we want to know why)
  - If it fails, and docker complains about the home directory:
    * Follow [this tutorial](https://dzone.com/articles/jenkins-02-changing-home-directory) to change Jenkins home directory to /home/jenkins
    * Also, might have to use command 'sudo chown -R jenkins:jenkins /home/jenkins' to update ownership of jenkins files
  - If it fails with "Got permission denied while trying to connect to Docker daemon ..." then: 
       "`
      sudo usermod -aG docker jenkins
      `" followed by:
      "`
       sudo service jenkins restart
        `" should do the trick.
  - If the script fails because of other reasons, that is expected
  - Push new change to Gerrit, and check in Jenkins that the analyzer\_trigger completes without error

### Setup Analyzer Executor and Jenkins RabbitMQ plugins:
  - Install Jenkins plugins: Go to Manage Jenkins->Manage Plugins->Available and install 'RabbitMQ Build Trigger Plugin' and 'RabbitMQ Consumer Plugin'
  - Configure plugins: Go to Manage Jenkins -> Configure System -> RabbitMQ Consumer:
    * Check 'Enable Consumer'
    * Service URI: 'amqp://guest:guest@localhost:5672'
    * Application ID: 'remote-build'
    * Queue name: 'svcmean.mean.analyzer-requests'
    * Test connection with 'Test Connection' button
    * Save changes
    * Note: Sometimes, the RabbitMQ Consumer Plugin stops consuming messages after a while. To fix this if/when it happens, go to Manage Jenkins -> Configure System -> RabbitMQ Consumer -> toggle 'Enable Consumer' off and on (and save in between).
  - Create Analyzer Executor job:
    * Create new item (folder) with name 'Services'
    * In 'Services' folder, create a new folder with name 'Mean'
    * In 'Mean' folder, create new item (pipeline), with name 'Analyzer\_executor\_pipeline'
      + Add Parameter: Configuration -> General -> This project is parameterized:
        - Add String Parameter:
          * Name: 'AnalyzeRequest'
          * Default Value: 'DEFAULT'
      + Add RabbitMQ trigger: Configuration -> Build Triggers -> RabbitMQ Build Trigger: Set Token to 'AESToken'
      + Add Script:
        - Create new git repo, copy analyzer\_executor/Jenkinsfile (the script at the end of this file) to repo, push (to Github for example)
        - In Jenkins, under 'Pipeline' in configuration
          + Select Definition: 'Pipeline script from SCM'
          + Select SCM: 'Git'
          + Select Repository URL: The git repo you just created
          + Set credentials as necessary
    * Commit change to Gerrit and make sure that analyzer executor runs: (for example, push a python script with a very long line to be sure that analysis will generate comment)
      + If you get RejectedAccessException, go to: Manage Jenkins -> In-process Script Approval, and accept pending approval.

### Done!

You should now have all the essential components up and running, and a push to Gerrit should result in robot comments being published to Gerrit as well. The following steps will go throught the steps to enable a few optional services, such as the storage publisher that stores analysis results.

### Setup database for analysis results
  - Install MongoDB
  - Start MongoDB with command: 'mongodb'

### Start Storage Publisher
  - Go to ~/go/bin
  - Run './storage\_publisher'
  - Enter 'guest' when prompted for password for RabbitMQ user 'guest'
  - Analysis results and 'not-useful' feedback should now get stored in the MongoDB database. Verify:
    * Push change to Gerrit
    * Start MongoDB interactive shell with command 'mongo' and run the following commands:
      + 'use mean'
      + 'db.published_notes.find()'
    * If the find() command returns a json object, it works, great job!

### Replacement analyzer\_executor/Jenkinsfile:

(NOTE: Replace PASSWORD with Gerrit PASSWORD)

```
import static Constants.*

class Constants {
    static final BASE_DIR = "mean"
    static final OUTPUT_DIR = BASE_DIR + "/output"
    static final RESULT_FILENAME = "result.json"
    static final HOST = "localhost"
    static final EXCHANGE = "meanexchange"
    static final QUEUE_NAME = "analyzer-events"
	static final PUBLISHER_IMAGE = "rabbitmq_publisher:latest"
}

@NonCPS
def parseJson(str) {
    return new groovy.json.JsonSlurperClassic().parseText(str)
}
def getEventTypeAndInfo(errorCode, stdErr) {
    if (errorCode == 124)
        return ['timeout', '']
    else if (errorCode != 0)
        return ['error', stdErr]
    else
        return ['result', '']
}
def createAnalyzerEvent(requestId, analyzerName, eventType, sourceContext, info='') {
    analyzerEvent = ['request_id': requestId,
                      'analyzer_name': analyzerName,
                      'event_type': eventType,
                      'source_context': sourceContext]

    if (info != '') {
        analyzerEvent['info'] = info
    }
    if (eventType == 'result') {
        if (fileExists(OUTPUT_DIR + "/" + RESULT_FILENAME)) {
            analyzerEvent['analyzer_result'] = parseJson(readFile(OUTPUT_DIR + "/" + RESULT_FILENAME))
        }
        else {
            analyzerEvent['event_type'] = 'error'
            analyzerEvent['info'] = 'no result file found, check that the analyzer finished correctly'
        }
    }
    return analyzerEvent
}


def req = parseJson(AnalyzeRequest)
def ctx = req.source_context
def seconds = req.containsKey("timeout") ? req.timeout : (1.0d / 0.0d)

node{
    stage('create input files') {
        checkout scm
        sh "rm -rf mean"
        sh "mkdir -p mean/code"
        sh "mkdir -p mean/input"
        sh "mkdir -p mean/output"
        sh 'echo $AnalyzeRequest > mean/input/analyze_request.json'
        analyzerStarted = createAnalyzerEvent(req.request_id, req.analyzer_name, "started", ctx)
        def jsonData = groovy.json.JsonOutput.toJson(analyzerStarted)
        writeFile(file: "analyzer_event.json", text: jsonData, encoding: "UTF-8")
        sh "docker run --network=\"host\" -v ${WORKSPACE}:/temp ${PUBLISHER_IMAGE} ${HOST} ${EXCHANGE} ${QUEUE_NAME} temp/analyzer_event.json"
        sh 'rm analyzer_event.json'
    }
    stage("checkout") {
    def (scheme, host) = ctx.host_uri.split("://", 2)
    sh "git clone ${scheme}://jenkins:PASSWORD@${host}/a/${ctx.project_name}.git mean/code"
    sh """
       cd mean/code
       git fetch ${scheme}://jenkins:PASSWORD@${host}/a/${ctx.project_name}.git ${ctx.ref} && git checkout FETCH_HEAD
       cd -
       """
    }
    stage("run analyzer") {
		def rc = sh(script: """
						  timeout ${seconds} docker run --rm\
                    --network=\"host\" \
									  -v \${WORKSPACE}/mean:/mean \
									  -u \$(id -u \${USER}):\$(id -g \${USER}) \
									  2> stderr.log \
									  ${req.docker_image}
						  """, returnStatus: true)
		def stderr = readFile("stderr.log")
		println("stderr: " + stderr + " END OF stderr")
		sh "rm stderr.log"
		def (eventType, info) = getEventTypeAndInfo(rc, stderr)
		analyzerEvent = createAnalyzerEvent(req.request_id, req.analyzer_name, eventType, ctx, info)
    }
    stage('publish') {
        def jsonData = groovy.json.JsonOutput.toJson(analyzerEvent)
        writeFile(file: "analyzer_event.json", text: jsonData, encoding: "UTF-8")
        sh "docker run --network=\"host\" -v ${WORKSPACE}:/temp ${PUBLISHER_IMAGE} ${HOST} ${EXCHANGE} ${QUEUE_NAME} temp/analyzer_event.json"
        sh 'rm analyzer_event.json'
    }
    stage('cleanup') {
        sh "rm -rf mean"
    }
}
```
