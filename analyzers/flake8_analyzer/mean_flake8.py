from subprocess import Popen, PIPE
from collections import namedtuple
import json
from pathlib import Path
import sys

Result = namedtuple('Result', 'stdout stderr returncode')

def run(*cmd):
    proc = Popen(cmd, stdout=PIPE, stderr=PIPE)
    stdout, stderr = proc.communicate()
    return Result(stdout=stdout.decode("utf-8"),
                  stderr=stderr.decode("utf-8"),
                  returncode=proc.returncode)

def flakeToMean(shChNote):
    return {
        "description": shChNote["text"],
        "category": str(shChNote["code"]),
        "location": {
            "path": shChNote["filename"][len("/mean/code/"):],
            "range": {
                "start_line": shChNote["line_number"],
                "start_column": shChNote["column_number"]
            }
        }
    }

def analyze(req):
    ignore = req.get("blacklisted_categories", [])
    paths = req["paths"]
    cmd = ["flake8", "--format=json"]
    if ignore:
        cmd += ["--extend-ignore="+",".join(ignore)]
    errors = []
    notes = []
    for path in paths:
        output = run(*cmd, path)
        if output.stderr:
            errors.append(output.stderr)

        try:
            flake8result = json.loads(output.stdout)
            print(json.dumps(flake8result, indent=4))
        except ValueError:
            errors.append("Could not parse json output from flake8.")
        else:
            notes += [flakeToMean(flakeNote) for _, flakeNotes in flake8result.items() for flakeNote in flakeNotes]

    return {"notes": notes, "errors": errors}


def main():
    with open("/mean/input/analyze_request.json", "r") as reqfile:
        req = json.load(reqfile)

    req["paths"] = ["/mean/code/" + p for p in req["paths"]]
    analyzerResult = analyze(req)

    with open("/mean/output/result.json", "w+") as resultFile:
        json.dump(analyzerResult, resultFile)

def main2():
    req = {"paths": sys.argv[1:], "blacklisted_categories": []}

    analyzerResult = analyze(req)

    print(json.dumps(analyzerResult, indent=4))

if __name__ == '__main__':
    main()
