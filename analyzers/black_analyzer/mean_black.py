from subprocess import Popen, PIPE
from collections import namedtuple
import json
from pathlib import Path
import os
import re
import difflib
import shutil
from difflib import *

Result = namedtuple("Result", "stdout stderr returncode")


def run(*cmd):
    proc = Popen(cmd, stdout=PIPE, stderr=PIPE)
    stdout, stderr = proc.communicate()
    return Result(
        stdout=stdout.decode("utf-8"),
        stderr=stderr.decode("utf-8"),
        returncode=proc.returncode,
    )


def blackToMean(description, ranges, path, fixSuggestionInfo):
    return {
        "description": description,
        "replacements": fixSuggestionInfo,
        "category": "formatting",
        "url": "(https://black.readthedocs.io/en/stable/the_black_code_style/current_style.html)",
        "location": {"path": path[len("/mean/code/") :], "range": ranges},
    }


def analyze(req):
    paths = req["paths"]

    notes = []
    errors = []
    for path in paths:
        output = run("black", "--diff", path)
        if output.stderr:
            errors.append(output.stderr)
        allLines = output.stdout.splitlines()
        if len(allLines) > 1:
            interval = allLines[2]
            lines = output.stdout.split("\n", 2)[-1]
            for match in re.finditer(r"^@@.*@@\n(.*\n)*?(?=@@|$)", lines, re.MULTILINE):
                initRow = int(
                    match.group().splitlines()[0].split(",")[0].split(" -")[1]
                )
                content = match.group().split("\n", 1)[-1]
                minusRegions, minusRanges = getRegions(
                    content, r"(^[-].*\n)+(^[-].*)*", initRow
                )
                plusRegions, _ = getRegions(content, r"(^[+].*\n)+(^[+].*)*", initRow)
                for index, value in enumerate(minusRegions):
                    replacement = re.sub(
                        r"^[+]", "", plusRegions[index], 0, re.MULTILINE
                    )
                    toList = open(path).read().split("\n")
                    startLine = minusRanges[index][0]
                    endLine = minusRanges[index][1]
                    lastElement = toList[endLine - 1]
                    lastLetters = replacement[len(replacement) - 1 : len(replacement)]
                    if lastLetters == "\n":
                        replacement = replacement[: len(replacement) - 1]
                    sc = 0

                    ec = len(lastElement)
                    fixReplacement = getReplacmentInfo(
                        path, startLine, sc, endLine, ec, replacement
                    )
                    fixSuggesstionIfo = getFixSuggesstionInfo(
                        "Formatting not being correct", fixReplacement
                    )
                    description = "Formatting not being correct according to Black"
                    ranges = {
                        "start_line": startLine,
                        "end_line": endLine,
                    }
                    notes += [
                        blackToMean(description, ranges, path, fixSuggesstionIfo)
                    ]

                # Get removed and added lines
                    removeAddMap = removeAddLinesToNote(path, content, initRow)
                    for _, value in removeAddMap.items():
                        notes.append(value[1])

    return {"notes": notes, "errors": errors}


def getAddBlankDescription(countLine):
    if countLine < 2:
        return "Add one line below"
    else:
        return "Add " + str(countLine) + " lines below"


def getRemoveBlankDescription(countLine):
    if countLine < 2:
        return "Remove this line"
    else:
        return "Remove these lines"


def removeAddLinesToNote(path, content, initRow):
    notesMap = dict()
    lines = open(path).read().split("\n")
    _, addLinesRanges = getAddOrRemoveLines(
        content, r"(^[+][ ]*\n)*(^[+][ ]*$)", initRow
    )
    for index, value in enumerate(addLinesRanges):
        countLine = value[1] - value[0] + 1
        description = getAddBlankDescription(countLine)
        lineNbr = value[0] - 1
        line = lines[lineNbr - 1]
        sc = ec = len(line)
        replacement = "".join(["\n" for item in range(0, countLine)])
        fixReplacement = getReplacmentInfo(
            path, lineNbr, sc, lineNbr, ec, replacement
        )
        ranges = {"start_line": lineNbr}
        fixSuggesstionIfo = getFixSuggesstionInfo(description, fixReplacement)
        notesMap[(lineNbr, lineNbr + countLine)] = (
            countLine,
            blackToMean(description, ranges, path, fixSuggesstionIfo),
        )

    # Remove lines
    regionsRemoveLines, removesLinesRanges = getAddOrRemoveLines(
        content, r"(^[-][ ]*\n)*(^[-][ ]*$)", initRow
    )
    for index, value in enumerate(removesLinesRanges):
        sc = len(lines[value[0] - 2])
        ec = len(lines[value[1] - 1])
        countLine = value[1] - value[0] + 1
        startLine = value[0] - 1
        endLine = value[1]
        replacement = ""
        ranges = {
            "start_line": value[0],
            "start_column": 0,
            "end_column": len(lines[value[1] - 1]),
            "end_line": value[1],
        }
        startsLines = [key[0] for key in list(notesMap.keys())]
        endLines = [key[1] for key in list(notesMap.keys())]
        print(startsLines)
        print(value[0])
        if (value[0], value[1]) in notesMap:
            diffCountLine = countLine - notesMap[value[0], value[1]][0]
            del notesMap[value[0]]
        elif value[1] in startsLines :
            index = startsLines.index(value[1])
            val = notesMap[value[1], endLines[index]]
            del notesMap[value[1], endLines[index]]
            diff = countLine - val[0]
            endLine = endLine - val[0]
            countLine = countLine - val[0]
            description = getRemoveBlankDescription(countLine)
            fixReplacement = getReplacmentInfo(
                path, startLine, sc, endLine, ec, replacement
            )
            fixSuggesstionIfo = getFixSuggesstionInfo(description, fixReplacement)
            ranges = {"start_line": value[0], "end_line": endLine}
            notesMap[(value[1], endLine)] = (
                countLine,
                blackToMean(description, ranges, path, fixSuggesstionIfo),
            )
        elif value[0] - 1 in startsLines :
            index = startsLines.index(value[0] - 1)
            val = notesMap[value[0] -1,endLines[index]]
            del notesMap[value[0] - 1 , endLines[index]]
            countLine = val[0] - countLine
            lineNbr = value[0] - 1
            line = lines[lineNbr - 1]
            sc = ec = len(line)
            replacement = "".join(["\n" for item in range(0, countLine)])
            fixReplacement = getReplacmentInfo(
                path, lineNbr, sc, lineNbr, ec, replacement
            )
            ranges = {"start_line": lineNbr}
            description = getAddBlankDescription(countLine)
            fixSuggesstionIfo = getFixSuggesstionInfo(description, fixReplacement)
            notesMap[(lineNbr, lineNbr + countLine)] = (
                countLine,
                blackToMean(description, ranges, path, fixSuggesstionIfo),
            )

        else:
            fixReplacement = getReplacmentInfo(
                path, startLine, sc, endLine, ec, replacement
            )
            description = getRemoveBlankDescription(countLine)
            fixSuggesstionIfo = getFixSuggesstionInfo(description, fixReplacement)
            notesMap[(value[0], value[1])] = (
                countLine,
                blackToMean(description, ranges, path, fixSuggesstionIfo),
            )
    return notesMap


def getAddOrRemoveLines(lines, regex, initRow):
    ranges = []
    regions = []
    for match in re.finditer(regex, lines, re.MULTILINE):
        start = getStartRow(lines, lines.count("\n", 0, match.start(0))) + initRow
        end = start + match.group().count("\n")
        ranges.append(((start, end)))
        regions.append(match.group())

    return regions, ranges


def getFixSuggesstionInfo(description, replacementInfo):

    return [
        {"description": description, "replacements": [replacementInfo]},
    ]


def getReplacmentInfo(path, startLine, sc, endLine, ec, replacement):
    return {
        "location": {
            "path": path[len("/mean/code/") :],
            "range": {
                "start_line": startLine,
                "start_column": sc,
                "end_column": ec,
                "end_line": endLine,
            },
        },
        "replacement": replacement,
    }


def getStartRow(lines, ind):
    count = 0
    for index, line in enumerate(lines.split("\n")[:ind]):
        if (not line.startswith("+")) and (not "BLACK_DONOT_COUNT" in line):
            count = count + 1
    return count


def removeBlankRows(regex, text, subst):
    for m in re.finditer(regex, text, re.MULTILINE):
        text = re.sub(regex, subst, text, 0, re.MULTILINE)
    return text


def getRegions(lines, regex, initRow):
    print(lines)
    lines = removeBlankRows(r"(^[-][ ]*)*(^[-][ ]*$)", lines, "")
    lines = removeBlankRows(r"(^[+][ ]*)*(^[+][ ]*$)", lines, "BLACK_DONOT_COUNT")
    matches = re.finditer(regex, lines, re.MULTILINE)
    regions = []
    ranges = []
    for match in matches:
        line = re.sub(r"^[+]", "", match.group(), 1, re.MULTILINE)
        start = getStartRow(lines, lines.count("\n", 0, match.start(0))) + initRow
        end = start + match.group().count("\n") - 1
        ranges.append(((start, end)))
        regions.append(match.group())
    return regions, ranges


def main():
    with open("/mean/input/analyze_request.json", "r") as reqfile:
        req = json.load(reqfile)

    req["paths"] = ["/mean/code/" + p for p in req["paths"]]
    analyzerResult = analyze(req)

    with open("/mean/output/result.json", "w+") as resultFile:
        json.dump(analyzerResult, resultFile)


if __name__ == "__main__":
    main()
