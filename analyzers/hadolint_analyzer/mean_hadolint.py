from subprocess import Popen, PIPE
from collections import namedtuple
import json
from pathlib import Path

Result = namedtuple('Result', 'stdout stderr returncode')

def run(*cmd):
    proc = Popen(cmd,
        stdout = PIPE,
        stderr = PIPE
    )
    stdout, stderr = proc.communicate()
    return Result(stdout=stdout.decode("utf-8"),
                  stderr=stderr.decode("utf-8"),
                  returncode=proc.returncode)

def hadoToMean(hadoNote):
    return {
        "description": hadoNote["message"],
        "category": hadoNote["code"],
        "location": {
            "path": hadoNote["file"][len("/mean/code/"):],
            "range": {
                "start_line": hadoNote["line"],
                # "start_column": hadoNote["column"]
            }
        }
    }

def analyze(req):
    ignore = req.get("blacklisted_categories", [])
    paths = req["paths"]

    cmd = ['./hadolint-Linux-x86_64']
    cmd += ['--format', 'json']
    for category in ignore:
        cmd += ['--ignore', category]

    errors = []
    notes = []
    for path in paths:
        output = run(*cmd, path)
        if output.stderr:
            errors.append(output.stderr)

        try:
            hadolintResult = json.loads(output.stdout)
        except ValueError as e:
            errors.append("Could not parse json output from hadolint.")
        else:
            notes += [hadoToMean(hadoNote) for hadoNote in hadolintResult]

    return {"notes": notes, "errors": errors}


def isDockerfile(path):
    p = Path(path)
    return p.stem.lower() == "dockerfile" or p.suffix.lower() == ".dockerfile"

def main():
    with open("/mean/input/analyze_request.json", "r") as reqfile:
        req = json.load(reqfile)

    req["paths"] = ["/mean/code/" + p for p in filter(isDockerfile, req["paths"])]
    analyzerResult = analyze(req)

    with open("/mean/output/result.json", "w+") as resultFile:
        json.dump(analyzerResult, resultFile)


if __name__ == '__main__':
    main()
