from bisect import bisect_left
from functools import lru_cache
from collections import namedtuple
from pathlib import Path
import re
import json
import sys

INITIAL_UPPER         = "C0001"
ENDING_PERIOD         = "C0002"
INITIAL_LOWER         = "C0003"
MISSING_ENDING_PERIOD = "C0004"

errorStrings = {
    INITIAL_UPPER:         "convention: Messages passed to this macro shall start with a lower case letter.",
    ENDING_PERIOD:         "convention: Messages passed to this macro shall not end with period (.).",
    INITIAL_LOWER:         "convention: Messages passed to this macro shall start with a upper case letter.",
    MISSING_ENDING_PERIOD: "convention: Messages passed to this macro shall end with period (.)."
}

macros = ["GST_ERROR",
          "GST_WARNING",
          "GST_INFO",
          "GST_DEBUG",
          "GST_LOG",
          "GST_FIXME",
          "GST_TRACE",
          "GST_ERROR_OBJECT",
          "GST_WARNING_OBJECT",
          "GST_INFO_OBJECT",
          "GST_DEBUG_OBJECT",
          "GST_LOG_OBJECT",
          "GST_FIXME_OBJECT",
          "GST_TRACE_OBJECT"]

macros2 = ["syserror",
           "error",
           "warning",
           "info",
           "g_error",
           "g_message",
           "g_critical",
           "g_warning",
           "g_info",
           "g_debug"]

macroRegex = re.compile(r'('+"|".join(macros)+r')\W*\(.*\".*?\".*\)')
macro2Regex = re.compile(r'('+"|".join(macros2)+r')\W*\(.*\".*?\".*\)')
stringRegex = re.compile(r"\"[^\"]*?\"")

@lru_cache(maxsize=8)
def index(char, text):
    return [i for i, c in enumerate(text) if c == char]

@lru_cache(maxsize=8)
def lineOf(charidx, text):
    return bisect_left(index('\n', text), charidx) + 1

def isAbbreviation(word):
    return len(word) > 1 and not any((ch.islower() for ch in word))

def getErrors(text):
    for match in macroRegex.finditer(text):
        m = stringRegex.search(match.group())
        if not m: continue

        sentence = m.group().strip('"')
        stringArg = sentence.strip()
        if not stringArg: continue

        firstWord = sentence.split(maxsplit=1)[0]

        if not isAbbrevation(firstWord) and stringArg[0].isupper():
            yield lineOf(match.start(), text), INITIAL_UPPER, errorStrings[INITIAL_UPPER]
        if stringArg[-1] == '.':
            yield lineOf(match.start(), text), ENDING_PERIOD, errorStrings[ENDING_PERIOD]

    for match in macro2Regex.finditer(text):
        m = stringRegex.search(match.group())
        if not m: continue

        stringArg = m.group().strip('"').strip()
        if not stringArg: continue

        if stringArg[0].islower():
            yield lineOf(match.start(), text), INITIAL_LOWER, errorStrings[INITIAL_LOWER]
        if stringArg[-1] != '.':
            yield lineOf(match.start(), text), MISSING_ENDING_PERIOD, errorStrings[MISSING_ENDING_PERIOD]


def analyzeFiles(filenames):
    for filename in filenames:
        with open(filename, "r") as f:
            for line, code, errstring in getErrors(f.read()):
                yield filename, line, code, errstring

def canCheck(path):
    p = Path(path)
    return p.suffix.lower() in {".c", ".h"}


def meanNote(path, errcode, description, startline):
    return {
        "description": description,
        "category": errcode,
        "location": {
            "path": path,
            "range": {
                "start_line": startline,
            }
        }
    }

def analyze(req):
    ignore = set(req.get("blacklisted_categories", []))
    paths = ("/mean/code/" + p for p in filter(canCheck, req["paths"]))

    return {"notes":
            [meanNote(f[len("/mean/code/"):], c, d, l)
            for f, l, c, d
            in analyzeFiles(paths)
            if c not in ignore]}

def main():
    with open("/mean/input/analyze_request.json", "r") as reqfile:
        req = json.load(reqfile)

    analyzerResult = analyze(req)

    with open("/mean/output/result.json", "w+") as resultFile:
        json.dump(analyzerResult, resultFile)

def main2():
    for filename, line, code, descr in analyzeFiles(sys.argv[1:]):
        print(meanNote(filename, code, descr, line))

if __name__ == "__main__":
    main()
    # main2()
