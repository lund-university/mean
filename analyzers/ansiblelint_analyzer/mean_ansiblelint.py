from subprocess import Popen, PIPE
from collections import namedtuple
import json
from pathlib import Path

Result = namedtuple('Result', 'stdout stderr returncode')

def run(*cmd):
    proc = Popen(cmd,
        stdout = PIPE,
        stderr = PIPE
    )
    stdout, stderr = proc.communicate()
    return Result(stdout=stdout.decode("utf-8"),
                  stderr=stderr.decode("utf-8"),
                  returncode=proc.returncode)

def ansiblelintToMean(ansibleLine):
    path, line, rest = ansibleLine.split(':', 2)
    code, desc = rest.strip().split(' ', 1)
    code = code[2:-1]
    return {
        "description": desc,
        "category": code,
        "location": {
            "path": path[len("/mean/code/"):],
            "range": {
                "start_line": int(line),
            }
        }
    }

def analyze(req):
    ignore = req.get("blacklisted_categories", [])
    paths = req["paths"]

    notes = []
    errors = []
    for path in paths: 
        output = run("ansible-lint", "-p", "-x", ','.join(ignore), path)
        if output.stderr:
            errors.append(output.stderr)
        notes += [ansiblelintToMean(line) for line in output.stdout.splitlines()]

    return {"notes": notes, "errors": errors}

def isAnsibleFile(path):
    p = Path(path)
    return p.suffix.lower() in {".yml", ".yaml"}

def main():
    with open("/mean/input/analyze_request.json", "r") as reqfile:
        req = json.load(reqfile)

    req["paths"] = ["/mean/code/" + p for p in filter(isAnsibleFile, req["paths"])]
    analyzerResult = analyze(req)

    with open("/mean/output/result.json", "w+") as resultFile:
        json.dump(analyzerResult, resultFile)

if __name__ == '__main__':
    main()
