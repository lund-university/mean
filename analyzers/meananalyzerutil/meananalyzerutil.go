package meananalyzerutil

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	msg "gitlab.com/lund-university/mean/message"
)

// directories
const (
	baseDir        = "/mean/"
	inputDir       = baseDir + "input/"
	outputDir      = baseDir + "output/"
	CodeDir        = baseDir + "code/"
	resultFile     = outputDir + "result.json"
	analyzeReqFile = inputDir + "analyze_request.json"
)

func AnalyzeFilesWithExt(analyze func(analyzeReq *msg.AnalyzeRequest) (*msg.AnalyzerResult, error), exts []string) error {
	analyzeReq, err := readAnalyzeRequest()
	if err != nil {
		return err
	}

	analyzeReq.Paths = pathsToAnalyzeWithExt(exts, analyzeReq.Paths)

	result, err := analyze(analyzeReq)
	if err != nil {
		return fmt.Errorf("Analyze error: %v", err)
	}

	prettyPrint(result)
	err = writeResult(result)
	if err != nil {
		return fmt.Errorf("Could not write to file: %v", err)
	}
	return nil
}

func readAnalyzeRequest() (*msg.AnalyzeRequest, error) {
	analyzeReq := &msg.AnalyzeRequest{}
	data, err := ioutil.ReadFile(analyzeReqFile)
	if err != nil {
		return analyzeReq, fmt.Errorf("Error reading %s: %v", analyzeReqFile, err)
	}
	if err := json.Unmarshal(data, analyzeReq); err != nil {
		return analyzeReq, fmt.Errorf("Could not unmarshal %s: %v", string(data), err)
	}
	return analyzeReq, nil
}

func writeResult(res *msg.AnalyzerResult) error {
	if err := os.MkdirAll(outputDir, os.ModePerm); err != nil {
		return fmt.Errorf("Could not create dir %s: %v", outputDir, err)
	}

	f, err := os.Create(resultFile)
	defer f.Close()
	if err != nil {
		return fmt.Errorf("Could not create file: %v", err)
	}

	resultJSON, err := json.MarshalIndent(res, "", "    ")
	if err != nil {
		return fmt.Errorf("Can not marshall: %v", err)
	}

	_, err = f.Write(resultJSON)
	if err != nil {
		return fmt.Errorf("Could not write: %v", err)
	}
	return nil
}

func pathsToAnalyzeWithExt(exts []string, paths []string) (filesWithExt []string) {
	for _, path := range paths {
		fileExt := filepath.Ext(path)
		for _, ext := range exts {
			if fileExt == ext {
				filesWithExt = append(filesWithExt, path)
				break
			}
		}
	}
	return
}

func prettyPrint(i interface{}) {
	s, err := json.MarshalIndent(i, "", "    ")
	if err != nil {
		fmt.Printf("{ CAN_NOT_PRETTYPRINT: {type: %T} }\n", i)
	}
	fmt.Println(string(s))
}
