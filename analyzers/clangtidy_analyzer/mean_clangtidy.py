from subprocess import Popen, PIPE
from collections import namedtuple
import json
from pathlib import Path
import os
import re
import difflib
import yaml
import shutil
from difflib import *

Result = namedtuple("Result", "stdout stderr returncode")


def run(*cmd):
    proc = Popen(cmd, stdout=PIPE, stderr=PIPE)
    stdout, stderr = proc.communicate()
    return Result(
        stdout=stdout.decode("utf-8"),
        stderr=stderr.decode("utf-8"),
        returncode=proc.returncode,
    )


def clangtidyToMean(row, description, fixSuggestionInfo, path, category, url):
    clangtidyNote = {
        "description": description,
        "replacements": fixSuggestionInfo,
        "url": url,
        "category": category,
        "location": {
            "path": path[len("/mean/code/") :],
            "range": {
                "start_line": row,
            },
        },
    }
    return clangtidyNote


def analyze(req):
    paths = req["paths"]
    notes = []
    errors = []
    ignore = req.get("blacklisted_categories", [])
    if ignore:
        ignore = [(",-" + ignore[0])] + ignore[1:]
    for path in paths:
        pathWithoutFileName = re.search(r"(^[\/].*)+([\/])", path).group()
        output = run(
            "clang-tidy",
            "--export-fixes=" + pathWithoutFileName + "/fix.yaml",
            "-checks=*" + ",-".join(ignore),
            path,
            "--",
        )
        if output.stderr:
            errors.append(output.stderr)

        flag = dict()
        with open(pathWithoutFileName + "/fix.yaml", "r") as yamlFile:
            jsonFile = yaml.load(yamlFile.read())
        with open(path) as codeFile:
            cppFile = codeFile.read()
        for diagnos in jsonFile["Diagnostics"]:
            description, row = jsonToNote(diagnos["DiagnosticMessage"], cppFile)
            category = diagnos["DiagnosticName"]
            if (description, row) in flag:
                flag[(description, row)].append(category)
            else:
                flag[(description, row)] = [category]
        for value in flag.values():
            value.sort()


        mapOfCategory = dict()
        for key, value in flag.items():
            mapOfCategory[value[0], key[1]] = value
        listOfFlags = [key[0] for key, _ in mapOfCategory.items()]
        run(
            "clang-tidy",
            "--export-fixes=" + pathWithoutFileName + "/fix.yaml",
            "-checks=" + ", ".join(listOfFlags),
            path,
            "--",
        )
        with open(pathWithoutFileName + "/fix.yaml", "r") as yamlFile:
            jsonFile = yaml.load(yamlFile.read())
        flagMap = dict()
        for diagnos in jsonFile["Diagnostics"]:
            description, row = jsonToNote(diagnos["DiagnosticMessage"], cppFile)
            if (diagnos["DiagnosticName"], row) in mapOfCategory:
                listOfCategory = mapOfCategory[diagnos["DiagnosticName"], row]
                message = diagnos["DiagnosticMessage"]
                replacements = message["Replacements"]
                fixSuggestionInfo = []
                if replacements:
                    fixReplacements = jsonToReplacements(replacements, path, cppFile)
                    fixSuggestionInfo = getFixSuggestionsInfo(
                        description, fixReplacements
                    )
                category = getCategory(sorted(listOfCategory))
                listOfUrl = [
                    "(https://clang.llvm.org/extra/clang-tidy/checks/"
                    + check
                    + ".html)"
                    for check in listOfCategory
                ]
                url = "\n".join(listOfUrl)
                notes += [
                    clangtidyToMean(
                        row, description, fixSuggestionInfo, path, category, url
                    )
                ]
    return {"notes": notes, "errors": errors}


def jsonToReplacements(replacements, path, cppFile):

    fixReplacements = []
    for replacement in replacements:
        offset = replacement["Offset"]
        length = replacement["Length"]
        replacementText = replacement["ReplacementText"]
        row = cppFile.count("\n", 0, offset) + 1
        startLine = row
        endLine = cppFile.count("\n", 0, offset + length) + 1
        startList = cppFile[:offset].split("\n")
        startColumn = len(startList[len(startList) - 1])
        endColumn = startColumn + length
        fixReplacements.append(
            getReplacmentInfo(
                path, startLine, startColumn, endLine, endColumn, replacementText
            )
        )
    return fixReplacements


def getCategory(ListCategory):
    if len(ListCategory) > 1:
        return "(" + ", ".join(ListCategory) + ")"
    else:
        return ListCategory[0]


def getFixSuggestionsInfo(description, fixReplacements):
    return [
        {"description": description, "replacements": fixReplacements},
    ]


def getReplacmentInfo(path, startLine, sc, endLine, ec, replacement):
    return {
        "location": {
            "path": path[len("/mean/code/") :],
            "range": {
                "start_line": startLine,
                "start_column": sc,
                "end_column": ec,
                "end_line": endLine,
            },
        },
        "replacement": replacement,
    }


def jsonToNote(diagnos, cppFile):
    description = diagnos["Message"]
    offset = int(diagnos["FileOffset"])
    row = cppFile.count("\n", 0, offset) + 1
    return description, row


def main():
    with open("/mean/input/analyze_request.json", "r") as reqfile:
        req = json.load(reqfile)

    req["paths"] = ["/mean/code/" + p for p in req["paths"]]
    analyzerResult = analyze(req)

    with open("/mean/output/result.json", "w+") as resultFile:
        json.dump(analyzerResult, resultFile)


if __name__ == "__main__":
    main()
