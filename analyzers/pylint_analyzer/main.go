package main

import (
	"encoding/json"
	"errors"
	"log"
	"os/exec"
	"strings"

	ma "gitlab.com/lund-university/mean/analyzers/meananalyzerutil"
	msg "gitlab.com/lund-university/mean/message"
)

const (
	analyzerName = "pylint"
	usageError   = "exit status 32"
	modulePrefix = "************* Module"
)

type (
	PylintIssue struct {
		Line    uint32 `json:"line,omitempty"`
		Column  uint32 `json:"column,omitempty"`
		Message string `json:"message,omitempty"`
		Symbol  string `json:"symbol,omitempty"`
		Path    string `json:"path,omitempty"`
	}
)

func analyze(analyzeReq *msg.AnalyzeRequest) (*msg.AnalyzerResult, error) {
	pyFiles := analyzeReq.Paths
	result := &msg.AnalyzerResult{}
	py3Files := getPython3Files(pyFiles)
	if len(py3Files) == 0 {
		return result, errors.New("No python3 files found")
	}
	for _, pyFile := range py3Files {
		cmd := exec.Command("pylint",
			"--output-format=json",
			"--disable="+strings.Join(analyzeReq.BlacklistedCategories, ","),
			ma.CodeDir+pyFile)
		buf, err := cmd.Output()

		switch err := err.(type) {
		case nil: // no issues. Do nothing.
		case *exec.ExitError: // issue discovered
			// pylint uses error codes to signal if there was an issue discovered.
			// However, a fatal message means that
			// pylint couldn't even finish processing. There may be partial
			// results though.
			if stderr := err.Stderr; len(stderr) != 0 {
				log.Println(string(stderr))
			}

			if err.Error() == usageError {
				return result, err
			}

			var pylintRes []PylintIssue
			if err := json.Unmarshal(buf, &pylintRes); err != nil {
				log.Printf("Could not unmarshal result")
				log.Println(string(buf))
				continue
			}

			for _, issue := range pylintRes {
				result.Notes = append(result.Notes,
					&msg.Note{
						Location: msg.Location{
							Path: pyFile,
							Range: &msg.TextRange{
								StartLine:   issue.Line,
								StartColumn: issue.Column,
							},
						},
						Description: issue.Message,
						Category:    issue.Symbol,
					})
			}

		case *exec.Error:
			return result, err
		default:
			return result, err
		}
	}
	return result, nil
}

func getPython3Files(pyFiles []string) []string {
	py3Files := []string{}
	for _, pyFile := range pyFiles {
		cmd := exec.Command("python",
			"-m",
			"py_compile", ma.CodeDir+pyFile)
		_, err := cmd.CombinedOutput()

		if err == nil {
			py3Files = append(py3Files, pyFile)
		}
	}
	return py3Files
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	err := ma.AnalyzeFilesWithExt(analyze, []string{".py"})
	if err != nil {
		log.Fatal(err)
	}
}
