from comment_parser import comment_parser
from pathlib import Path
import sys
import json

CONV_ONLY_MULTILINE = "C0001"
CONV_NO_TEXT        = "C0002"
CONV_INITIAL_UPPER  = "C0003"
CONV_ENDING_PERIOD  = "C0004"

errorStrings = {
    CONV_ONLY_MULTILINE: "convention: Use no //-style comments, only /* */ and /** */.",
    CONV_NO_TEXT:        "Comment contain no text.",
    CONV_INITIAL_UPPER:  "convention: First letter in comment shall be lower case.",
    CONV_ENDING_PERIOD:  "convention: Comment shall not end with period (.)."
}

def checkFile(path, checkerfunc, mime, ignore=set()):
    notes = []
    for comment in comment_parser.extract_comments(path, mime=mime):
        errcodes = checkerfunc(comment)
        for errcode in errcodes:
            if errcode not in ignore:
                commentlen = comment.text().count('\n')
                endline = comment.line_number()+commentlen
                notes.append(meanNote(path, errcode, comment.line_number(), endline))
    return notes


def meanNote(path, errcode, startline, endline = None):
    return {
        "description": errorStrings[errcode],
        "category": errcode,
        "location": {
            "path": path,
            "range": {
                "start_line": startline,
                "end_line": endline
            }
        }
    }

def checkComment(comment):
    if not comment.is_multiline():
        return [CONV_ONLY_MULTILINE]

    text = comment.text().strip("*").strip()

    if not text:
        return [CONV_NO_TEXT]

    # Don't check /** */ comments
    if comment.text()[0] == '*':
        return []

    errcodes = []

    if text[0].isupper():
        firstWord = text.split(" ", 1)[0]
        if len(firstWord) == 1 or not firstWord.isupper():
            errcodes.append(CONV_INITIAL_UPPER)

    if text[-1] is '.':
        errcodes.append(CONV_ENDING_PERIOD)

    return errcodes


def analyze(req):
    ignore = set(req.get("blacklisted_categories", []))
    paths = ["/mean/code/" + p for p in filter(canCheck, req["paths"])]

    notes = []
    for path in paths:
        notes += checkFile(path,
                           checkerfunc=checkComment,
                           mime="text/x-c",
                           ignore=ignore)

    for note in notes:
        note["location"]["path"] = note["location"]["path"][len("/mean/code/"):]

    return {"notes": notes}


def canCheck(path):
    p = Path(path)
    return p.suffix.lower() in {".c", ".h"}

def main():
    with open("/mean/input/analyze_request.json", "r") as reqfile:
        req = json.load(reqfile)

    analyzerResult = analyze(req)

    with open("/mean/output/result.json", "w+") as resultFile:
        json.dump(analyzerResult, resultFile)


if __name__ == '__main__':
    main()
