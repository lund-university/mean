from subprocess import Popen, PIPE
from collections import namedtuple
import json
from pathlib import Path

Result = namedtuple('Result', 'stdout stderr returncode')

def run(*cmd):
    proc = Popen(cmd,
        stdout = PIPE,
        stderr = PIPE
    )
    stdout, stderr = proc.communicate()
    return Result(stdout=stdout.decode("utf-8"),
                  stderr=stderr.decode("utf-8"),
                  returncode=proc.returncode)

def shChToMean(shChNote):
    return {
        "description": shChNote["message"],
        "category": str(shChNote["code"]),
        "location": {
            "path": shChNote["file"][len("/mean/code/"):],
            "range": {
                "start_line": shChNote["line"],
                "start_column": shChNote["column"]
            }
        }
    }

def analyze(req):
    ignore = req.get("blacklisted_categories", [])
    paths = req["paths"]
    cmd = ["shellcheck", "-f", "json"]
    if ignore:
        cmd += ["-e", ",".join(ignore)]
    errors = []
    notes = []
    for path in paths:
        output = run(*cmd, path)
        if output.stderr:
            errors.append(output.stderr)

        try:
            hadolintResult = json.loads(output.stdout)
        except ValueError:
            errors.append("Could not parse json output from hadolint.")
        else:
            notes += [shChToMean(shChNote) for shChNote in hadolintResult]

    return {"notes": notes, "errors": errors}


def main():
    with open("/mean/input/analyze_request.json", "r") as reqfile:
        req = json.load(reqfile)

    req["paths"] = ["/mean/code/" + p for p in req["paths"]]
    analyzerResult = analyze(req)

    with open("/mean/output/result.json", "w+") as resultFile:
        json.dump(analyzerResult, resultFile)


if __name__ == '__main__':
    main()
