# Storage Publisher

This micro-service is responsible for storing analysis results, robot comments, and 'not-useful' feedback in a [MongoDB database](https://www.mongodb.com/). This is done by listening to several RabbitMQ queues that the [Robot Publisher](../robot_publisher/main.go) and the [Not Useful Server](../not-useful-server/not-useful-server.py) send messages to.

## Start Service

To be able to start the service, a few things need to be done.

### Code Modifications

To get the service running, first of all a few constants need to be filled out:
  * RMQ\_HOST: The RabbitMQ host name
  * RMQ\_PORT: The RabbitMQ port number
  * MONGODB\_HOST: The MongoDB host name
  * MONGODB\_PORT: The MongoDB port number
  * DATABASE: The name of the MongoDB database where the recieved analysis results will be stored

Also, if you are running RabbitMQ with SSL, the character 's' will need to be appended after 'amqp' in the definition of the 'rmqURI' variable.

### Command-line Arguments

After the constants are filled out and the program is compiled, it can be started in a console with the command:

```
./storage_publisher -rmquser RMQ_USER_HERE -mongodbuser MONGODB_USER_HERE
```

Where:
  * -rmquser: The RabbitMQ user
  * -mongodbuser: The MongoDB user (this argument can be skipped if authentication is not enabled for the MongoDB database)

After the service has started, the user will be prompted to enter the RabbitMQ password associated with the RabbitMQ username, and the MongoDB password associated with the MongoDB username (if a MongoDB username was specified).

### Local Setup

An example of how the constants and command-line arguments can look for a local setup:

Constants:
  * RMQ\_HOST     = "localhost"
  * RMQ\_PORT     = "5672"
  * MONGODB\_HOST = "localhost"
  * MONGODB\_PORT = "27017"
  * DATABASE      = "mean"

Command:

```
./storage_publisher -rmquser guest
```

And last of all, enter 'guest' when prompted for RabbitMQ password.
