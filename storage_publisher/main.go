package main

import (
  "fmt"
  "log"
  "time"
  "encoding/json"
  "context"
  "strings"
  "flag"
  "syscall"

  rmq "gitlab.com/lund-university/mean/mean_components/util/rabbitmqutil"

  "github.com/streadway/amqp"
  "go.mongodb.org/mongo-driver/mongo"
  "go.mongodb.org/mongo-driver/mongo/options"
  "golang.org/x/crypto/ssh/terminal"
)

func failOnError(msg string, err error) {
  if err != nil {
    log.Fatalln(msg, ": ", err)
  }
}

func printOnError(msg string, err error) {
  if err != nil {
    log.Println(msg, ": ", err)
  }
}

func main() {
  // Constants
  const RMQ_HOST = "localhost" // RabbitMQ host
  const RMQ_PORT = "5672" // RabbitMQ port
  const MONGODB_HOST = "localhost" // MongoDB host
  const MONGODB_PORT = "27017" // MongoDB port
  const DATABASE = "mean" // MongoDB database name

  // Command-line arguments
  rmqUser := flag.String("rmquser", "guest", "RabbitMQ user")
  mongoDBUser := flag.String("mongodbuser", "", "MongoDB user")
  flag.Parse()

  // Passwords
  var mongoDBPassword []byte;
  fmt.Printf("RabbitMQ password for \"%s\": ", *rmqUser)
  rmqPassword, err := terminal.ReadPassword(syscall.Stdin)
  fmt.Println()
  failOnError("Error when reading password", err)
  if *mongoDBUser != "" {
    fmt.Printf("MongoDB password for \"%s\": ", *mongoDBUser)
    mongoDBPassword, err = terminal.ReadPassword(syscall.Stdin)
    fmt.Println()
    failOnError("Error when reading password", err)
  }

  fmt.Println("Start Storage-publisher Service")

  // URIs
  var mongoDBURI string;
  rmqURI := fmt.Sprintf("amqp://%s:%s@%s:%s/", *rmqUser, rmqPassword, RMQ_HOST, RMQ_PORT)
  if *mongoDBUser != "" {
    mongoDBURI = fmt.Sprintf("mongodb://%s:%s@%s:%s", *mongoDBUser, mongoDBPassword, MONGODB_HOST, MONGODB_PORT)
  } else {
    mongoDBURI = fmt.Sprintf("mongodb://%s:%s", MONGODB_HOST, MONGODB_PORT)
  }

  conn, err := amqp.Dial(rmqURI)

  failOnError("RabbitMQ connection failed", err)

  defer conn.Close()

  consumer := rmq.NewSimpleConsumer(conn, "svcmean.mean.storage")

  opts := options.Client().ApplyURI(mongoDBURI)
  client, err := mongo.Connect(context.TODO(), opts)

  failOnError("MongoDB connection failed", err)

  err = client.Ping(context.TODO(), nil);

  failOnError("MongoDB ping failed", err);

  database := client.Database(DATABASE)

  go consume(consumer, database)
  select {}
}

func consume(consumer rmq.Consumer, database *mongo.Database) {
  for {
    msgs, err := consumer.Consume()
    if err != nil {
      fmt.Printf("Could not consume: %v", err)
      fmt.Println("Trying again ...")
      time.Sleep(time.Duration(time.Second))
      continue
    }

    for m := range msgs {
      go handleMessage(m, database.Collection(strings.Replace(m.RoutingKey, "-", "_", -1)))
    }
  }
}

func handleMessage(message amqp.Delivery, coll *mongo.Collection) {
  log.Println("Received Message:")
  log.Println("---------------------------")
  log.Printf(string(message.Body))
  log.Println("---------------------------")
  var received interface{}

  err := json.Unmarshal(message.Body, &received)

  printOnError("Failed to unmarshal bytes", err)

  _, err = coll.InsertOne(context.TODO(), received)

  printOnError("Error when inserting into database", err)
}
