docker run -it \
           -p 5000:5000 \
           -v $NOT_USEFUL_SERVER_INPUT_DIR:/input:ro \
           $NOT_USEFUL_SERVER_IMAGE \
           "0.0.0.0" \
           username  \
           password \
           certificate.crt \
           keypair.key \
           $RMQ_HOST
