from flask import Flask, Response, request
import sys
import pika
import ssl
import getpass

app = Flask(__name__)

PORT = /* replace */
EXCHANGE = /* replace */
ROUTING_KEY = /* replace */

def getChannel():
    while True:
        try:
            return conn.channel()
        except:
            conn = pika.BlockingConnection(parameters)


def publish_not_useful(data):
    channel = getChannel()
    channel.basic_publish(exchange=EXCHANGE,
                          routing_key=ROUTING_KEY,
                          body=data)
    print('SENT [x] %r: "%r"' % (ROUTING_KEY, data))
    channel.close()

@app.route('/', methods=['POST','OPTIONS'])
def index():
    resp = Response(")]}\'{}")
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Access-Control-Allow-Methods'] = 'POST, OPTIONS'
    resp.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'
    if request.method == 'OPTIONS':
        return resp
    if request.method == 'POST':
        publish_not_useful(request.data)
        return resp

if __name__ == '__main__':

    if len(sys.argv) == 6:
        host = sys.argv[1]
        username = sys.argv[2]
        password = getpass.getpass("password: ")
        certfile = sys.argv[3]
        keyfile = sys.argv[4]
        rmqhost = sys.argv[5]
        credentials = pika.PlainCredentials(username, password)
        ssl_options = pika.SSLOptions(context=ssl.SSLContext(ssl.PROTOCOL_TLSv1))
        parameters = pika.ConnectionParameters(host=rmqhost,
                                               port=PORT,
                                               ssl_options=ssl_options,
                                               credentials=credentials)
    else:
        print(len(sys.argv), "is not correct number of arguments!")
        sys.exit(1)

    conn = pika.BlockingConnection(parameters)

    app.run(host=host,
            debug=False,
            ssl_context=(certfile, keyfile))

    conn.close()
