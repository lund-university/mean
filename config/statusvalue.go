package config

import (
	"bytes"
	"encoding/json"
	"fmt"
)

type StatusType uint8

const (
	NotConfigured StatusType = iota
	Block
	Disable
	Enable
	Force
)

func (s StatusType) String() string {
	return toString[s]
}

var toString = map[StatusType]string{
	NotConfigured: "not_configured",
	Block:         "block",
	Disable:       "disable",
	Enable:        "enable",
	Force:         "force",
}

var toState = inverse(toString)

func inverse(m map[StatusType]string) map[string]StatusType {
	n := make(map[string]StatusType)
	for k, v := range m {
		n[v] = k
	}
	return n
}

// UnmarshalYAML unmashals a quoted YAML string to the enum value
func (s *StatusType) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var str string
	if err := unmarshal(&str); err != nil {
		return fmt.Errorf("Can not unmarshal StatusType: %v", err)
	}
	status, ok := toState[str]
	if !ok {
		return fmt.Errorf("Can not unmarshal \"%s\" to StatusType", str)
	}
	*s = status
	return nil
}

func (s StatusType) MarshalYAML() (interface{}, error) {
	return s.String(), nil
}

func (s StatusType) MarshalJSON() ([]byte, error) {
	str, ok := toString[s]
	if !ok {
		return nil, fmt.Errorf("Can not marshal AnalyzerState: %d", s)
	}
	buffer := bytes.NewBufferString(`"`)
	buffer.WriteString(str)
	buffer.WriteString(`"`)
	return buffer.Bytes(), nil
}

// UnmarshalJSON unmashals a quoted json string to the enum value
func (s *StatusType) UnmarshalJSON(b []byte) error {
	var str string
	err := json.Unmarshal(b, &str)
	if err != nil {
		return err
	}
	event, ok := toState[str]
	if !ok {
		return fmt.Errorf("Can not unmarshal \"%s\" to AnalyzerEvent", str)
	}
	*s = event
	return nil
}
