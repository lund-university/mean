package config

import (
	"encoding/json"
	"fmt"
)

type void struct{}

var nothing void

type StringSet map[string]struct{}

func (set StringSet) add(str string) {
	set[str] = nothing
}

func NewStringSet(strs ...string) StringSet {
	set := StringSet{}
	for _, s := range strs {
		set.add(s)
	}
	return set
}

func (set StringSet) Contains(str string) bool {
	_, ok := set[str]
	return ok
}

func (set *StringSet) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var strs []string
	if err := unmarshal(&strs); err != nil {
		return fmt.Errorf("Can not unmarshal StringSet: %v", err)
	}
	*set = NewStringSet(strs...)
	return nil
}

func (set *StringSet) UnmarshalJSON(b []byte) error {
	var strs []string
	err := json.Unmarshal(b, &strs)
	if err != nil {
		return fmt.Errorf("Can not unmarshal StringSet: %v", err)
	}
	*set = NewStringSet(strs...)
	return nil
}
