package config

import (
	"encoding/json"
	"fmt"
	"log"
	"testing"

	yaml "gopkg.in/yaml.v3"
)

const logExpectedErrors = false

var (
	globalConfig = GlobalConfig{
		"coverity": GlobalAnalyzerConfig{
			Image:   "coverity_image",
			Status:  Disable,
			Regex:   `^.+\.(java|cc|cpp)`,
			TimeOut: 3,
		},
		"pylint": GlobalAnalyzerConfig{
			Image:   "pylint_image",
			Status:  Block,
			Regex:   `^.+\.(py)`,
			TimeOut: 1,
		},
	}

	localConfig = LocalConfig{
		"coverity": LocalAnalyzerConfig{
			Status:  Enable,
			Regex:   `^.+\.(h|cc|c)`,
			TimeOut: 5,
		},
		"pylint": LocalAnalyzerConfig{
			Status:  Enable,
			Regex:   `^.+\.(pyc)`,
			TimeOut: 0,
		},
	}
)

func logError(testName string, err error) {
	if logExpectedErrors && err != nil {
		log.Printf("%s expected error: %v", testName, err)
	}
}

func TestReadValidConf(t *testing.T) {
	_, err := readGlobalConf("testconfigs/valid1.yml")
	if err != nil {
		t.Error(err)
	}
}

func TestNoStatus(t *testing.T) {
	conf, err := readGlobalConf("testconfigs/nostatus.yml")
	if err != nil {
		t.Error(err)
	}
	err = checkValidGlobalConfig(conf)
	logError(t.Name(), err)
	if err == nil {
		t.Errorf("No error even though NO status set in: \n %+v", conf)
	}
}

func TestNoImage(t *testing.T) {
	conf, err := readGlobalConf("testconfigs/noimage.yml")
	if err != nil {
		t.Error(err)
	}
	err = checkValidGlobalConfig(conf)
	logError(t.Name(), err)
	if err == nil {
		t.Errorf("No error even though NO image set in: \n %+v", conf)
	}
}

func TestNoSuffixes(t *testing.T) {
	conf, err := readGlobalConf("testconfigs/nosuffixes.yml")
	if err = checkValidGlobalConfig(conf); err == nil {
		t.Errorf("No error even though NO suffixes set in: \n %+v", conf)
	}
}

func TestBadRegex(t *testing.T) {
	conf, err := readGlobalConf("testconfigs/badregex.yml")
	if err = checkValidGlobalConfig(conf); err == nil {
		t.Errorf("No error even though NO suffixes set in: \n %+v", conf)
	}
	logError(t.Name(), err)
}

func TestNoTimeOut(t *testing.T) {
	conf, err := readGlobalConf("testconfigs/notimemout.yml")

	if err = checkValidGlobalConfig(conf); err != nil {
		t.Errorf("error even though NO timeout set in: \n %+v", conf)
	}
	if conf["coverity"].TimeOut != 0 {
		t.Errorf("TimeOut is not 0 even though NO timeout set in: \n %+v", conf)
	}
}

func configsEqual(merged, correct GlobalAnalyzerConfig) bool {
	if merged.Image != correct.Image {
		return false
	}
	if merged.Regex != correct.Regex {
		return false
	}

	if merged.LocalRegex != correct.LocalRegex {
		return false
	}

	if merged.Status != correct.Status {
		return false
	}

	if merged.TimeOut != correct.TimeOut {
		return false
	}
	return true
}

func TestMergeConfigWithEnable(t *testing.T) {

	merged, _ := MergeConfigs(globalConfig, localConfig)
	correct := GlobalConfig{
		"coverity": GlobalAnalyzerConfig{
			Image:   "coverity_image",
			Status:  Enable,
			Regex:   `^.+\.(cc)`,
			TimeOut: 5,
		},
		"pylint": GlobalAnalyzerConfig{
			Image:   "pylint_image",
			Status:  Block,
			Regex:   `^.+\.(py)`,
			TimeOut: 1,
		}}
	isCorrect := true
	for k, v := range merged {
		if configsEqual(v, correct[k]) {
			isCorrect = false
		}
	}
	if isCorrect {
		t.Errorf("configs are not merged correctly: \n %+v", merged)
	}
}

func TestMergeConfigWithOutLocal(t *testing.T) {

	merged, _ := MergeConfigs(globalConfig, LocalConfig{})
	correct := globalConfig
	isCorrect := true
	for k, v := range merged {
		if configsEqual(v, correct[k]) {
			isCorrect = false
		}
	}
	if isCorrect {
		t.Errorf("configs are not merged correctly with empty local: \n %+v", merged)
	}
}

func TestReadLocal(t *testing.T) {
	local, err := readLocalConf("testconfigs/validlocal1.yml")
	if err != nil {
		t.Error(err)
	}
	if err = checkValidLocalConfig(local); err != nil {
		t.Errorf("Expected valid config")
	}
	global, err := readGlobalConf("testconfigs/valid2.yml")
	if err != nil {
		t.Error(err)
	}
	if err = checkValidGlobalConfig(global); err != nil {
		t.Errorf("Expected valid config")
	}
	merged, _ := MergeConfigs(global, local)
	out, err := yaml.Marshal(merged)
	if err != nil {
		t.Errorf("Unmarshall error: %v", err)
	}

	expected := `analyze1:
    image: analyze1_image
    timeout: 5
    regex: ^+\.(cc)$
    status: enable
    localregex: ^+\.(cc)$
`

	if string(out) != expected {
		t.Error("Output not expected")
		t.Log(string(out))
	}

}

func TestBadConfigLocal(t *testing.T) {
	conf, err := readLocalConf("testconfigs/localbadstatus.yml")
	if err != nil {
		t.Error(err)
	}
	err = checkValidLocalConfig(conf)
	logError(t.Name(), err)
	if err == nil {
		t.Errorf("Expected error bad status")
	}
}

func TestValidCategories(t *testing.T) {
	conf, err := readGlobalConf("testconfigs/validcategories.yml")
	if err != nil {
		t.Error(err)
	}

	if !(conf["analyze1"].Categories["warning"] == Disable) ||
		!(conf["analyze1"].Categories["convention"] == Force) {
		t.Error("Categories are not read properly")
	}
	if len(conf["analyze1"].Categories) != 2 {
		t.Error("Categories does not have expected length")
	}
}

func TestValidBlacklistJson(t *testing.T) {
	type Config struct {
		Blacklist StringSet `json:"blacklisted_categories"`
	}
	data := []byte(`{ "blacklisted_categories": ["warning", "convention"] }`)
	var conf Config
	if err := json.Unmarshal(data, &conf); err != nil {
		t.Error(err)
	}
	ok := conf.Blacklist.Contains("warning") && conf.Blacklist.Contains("convention")
	if !ok {
		t.Error("Blacklist does not contain expected categories")
		t.Log(conf)
	}
	if len(conf.Blacklist) != 2 {
		t.Error("Blacklist does not have expected length")
		t.Log(conf)
	}
}

func TestMergeWithBlacklist(t *testing.T) {
	global, err := readGlobalConf("testconfigs/validglobal2.yml")
	if err != nil {
		t.Error(err)
	}
	err = checkValidGlobalConfig(global)
	if err != nil {
		t.Error(err)
	}
	local, err := readLocalConf("testconfigs/validlocal2.yml")
	if err != nil {
		t.Error(err)
	}
	err = checkValidLocalConfig(local)
	if err != nil {
		t.Error(err)
	}
	merged, err := MergeConfigs(global, local)
	if err != nil {
		t.Error(err)
	}

	result := fmt.Sprintf("%+v", merged["analyze1"].Categories)
	expected := `map[cat1:block cat2:disable cat3:disable cat4:force cat5:enable imnotinglobal:disable]`
	if result != expected {
		t.Error("Categories not merged correctly")
		t.Logf("Expected: %s\n", expected)
		t.Logf("Result: %s\n", result)
	}
}

func TestLocalJSON(t *testing.T) {
	js := []byte(`{"pylint": {"status": "disable"}}`)
	var c LocalConfig
	err := json.Unmarshal(js, &c)
	if err != nil {
		t.Error(err)
	}
	fmt.Printf("%+v", c)

}
