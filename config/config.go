package config

import (
	"fmt"
	"io/ioutil"
	"regexp"

	yaml "gopkg.in/yaml.v3"
)

const configPath = "config.yaml"

type (
	GlobalConfig map[string]GlobalAnalyzerConfig

	GlobalAnalyzerConfig struct {
		Image      string                `yaml:"image"`
		TimeOut    uint32                `yaml:"timeout,omitempty"`
		Regex      string                `yaml:"regex"`
		Status     StatusType            `yaml:"status"`
		Categories map[string]StatusType `yaml:"categories"`
		LocalRegex string
	}

	LocalConfig map[string]LocalAnalyzerConfig

	LocalAnalyzerConfig struct {
		TimeOut             uint32     `yaml:"timeout,omitempty"              json:"timeout,omitempty"`
		Regex               string     `yaml:"regex,omitempty"                json:"regex,omitempty"`
		Status              StatusType `yaml:"status,omitempty"               json:"status,omitempty"`
		BlacklistCategories StringSet  `yaml:"blacklist_categories,omitempty" json:"blacklist_categories,omitempty"`
	}
)

func LoadConfig(path string) GlobalConfig {
	config, err := readGlobalConf(path)

	if err != nil {
		panic(err)
	}

	if err = checkValidGlobalConfig(config); err != nil {
		panic(err)
	}

	return config
}

func checkValidGlobalConfig(config GlobalConfig) error {
	prefix := "invalid global config: "
	for k, analyzerConf := range config {
		if analyzerConf.Image == "" {
			return fmt.Errorf(prefix+"image need to be configured for %v", k)
		}
		if analyzerConf.Regex == "" {
			return fmt.Errorf(prefix+"regex need to be configured for %v", k)
		}

		if _, err := regexp.Compile(analyzerConf.Regex); err != nil {
			return fmt.Errorf(prefix+"regex `%s` does not compile for %v", analyzerConf.Regex, k)
		}
		if analyzerConf.Status == NotConfigured {
			return fmt.Errorf(prefix+"status need to be configured for %v", k)
		}
	}

	return nil
}

func checkValidLocalConfig(config LocalConfig) error {
	for analyzerName, analyzerConf := range config {
		switch analyzerConf.Status {
		case NotConfigured, Block, Force:
			return fmt.Errorf("Unallowed status `%s` for `%s`", analyzerConf.Status, analyzerName)
		case Enable, Disable: // OK
		default:
			return fmt.Errorf("Unknown status `%v` for `%s`", analyzerConf.Status, analyzerName)
		}
		if analyzerConf.Regex != "" {
			_, err := regexp.Compile(analyzerConf.Regex)
			if err != nil {
				return fmt.Errorf("regex `%s` does not compile for %v", analyzerConf.Regex, analyzerName)
			}
		}
	}
	return nil
}

func readGlobalConf(path string) (GlobalConfig, error) {
	var config GlobalConfig
	yamlFile, err := ioutil.ReadFile(path)
	if err != nil {
		return config, fmt.Errorf("Error reading %s: %v", path, err)
	}
	err = yaml.Unmarshal(yamlFile, &config)
	if err != nil {
		return config, fmt.Errorf("Unmarshal error: %v", err)
	}
	return config, nil
}

func readLocalConf(path string) (LocalConfig, error) {
	var config LocalConfig
	yamlFile, err := ioutil.ReadFile(path)
	if err != nil {
		return config, fmt.Errorf("Error reading %s: %v", path, err)
	}
	err = yaml.Unmarshal(yamlFile, &config)
	if err != nil {
		return config, fmt.Errorf("Unmarshal error: %v", err)
	}
	return config, nil
}

// func contains(list []string, a string) bool {
// 	for _, e := range list {
// 		if a == e {
// 			return true
// 		}
// 	}
// 	return false
// }
// func interSection(a, b []string) []string {
// 	var c []string
// 	for _, e := range a {
// 		if contains(b, e) {
// 			c = append(c, e)
// 		}
// 	}
// 	return c
// }

func mergeAnalyzerConfigs(analyzerConfig GlobalAnalyzerConfig, localAnalyzerConfig LocalAnalyzerConfig) GlobalAnalyzerConfig {
	switch analyzerConfig.Status {
	case Force, Block:
	case Enable, Disable:
		if localAnalyzerConfig.Status != NotConfigured {
			analyzerConfig.Status = localAnalyzerConfig.Status
		}
		if localAnalyzerConfig.TimeOut != 0 {
			analyzerConfig.TimeOut = localAnalyzerConfig.TimeOut
		}
		analyzerConfig.LocalRegex = localAnalyzerConfig.Regex
	default:
		fmt.Println("Not configured")
	}

	if analyzerConfig.Categories == nil {
		analyzerConfig.Categories = make(map[string]StatusType)
	}

	for c := range localAnalyzerConfig.BlacklistCategories {
		status, ok := analyzerConfig.Categories[c]
		if !ok {
			analyzerConfig.Categories[c] = Disable
			continue
		}
		switch status {
		case Force, Block, Disable:
		case Enable:
			analyzerConfig.Categories[c] = Disable
		default:
			fmt.Println("Unexpected status for analyzerCategory")
		}
	}
	return analyzerConfig
}

func MergeConfigs(globalConfig GlobalConfig, localConfig LocalConfig) (GlobalConfig, error) {
	err := checkValidLocalConfig(localConfig)
	if err != nil {
		return globalConfig, fmt.Errorf("Invalid local config: %v. Using global config instead.", err)
	}
	config := GlobalConfig{}
	for k, v := range globalConfig {
		if val, ok := localConfig[k]; ok {
			config[k] = mergeAnalyzerConfigs(v, val) //globalConfig[k] = v.mergeConfigs(val)
		} else {
			config[k] = v
		}
	}
	return config, nil
}
