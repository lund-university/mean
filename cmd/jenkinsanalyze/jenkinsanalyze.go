package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"syscall"

	"github.com/streadway/amqp"
	"gitlab.com/lund-university/mean/mean"
	"golang.org/x/crypto/ssh/terminal"

	"gitlab.com/lund-university/mean/mean_components/analyzerequestlistener/rmqrequestlistener"
	"gitlab.com/lund-university/mean/mean_components/analyzereventlistener/rmqeventlistener"
	"gitlab.com/lund-university/mean/mean_components/analyzerexecutor/rmqtriggerexecutor"
	"gitlab.com/lund-university/mean/mean_components/analyzerstatehandler/leveldbstate"
	"gitlab.com/lund-university/mean/mean_components/meaneventstreamer/rmqeventstreamer"
	"gitlab.com/lund-university/mean/mean_components/util/rabbitmqutil"
	msg "gitlab.com/lund-university/mean/message"
)

func main() {
	const (
		rmqPort  = 0  //replace
		EXCHANGE = "" //replace
		vHost = "" //replace
	)
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	configPath := flag.String("config", "config.yaml", "Path to configuration file")
	rmqHost := flag.String("rmqhost", "NO_HOST", "RabbitMQ host")
	rmqUser := flag.String("rmquser", "NO_USER", "RabbitMQ user")
	flag.Parse()

	fmt.Printf("Password for \"%s\": ", *rmqUser)
	password, err := terminal.ReadPassword(syscall.Stdin)
	fmt.Println()
	failIfErr(err)

	rmqURL := fmt.Sprintf("amqps://%s:%s@%s:%s/%s", *rmqUser, password, *rmqHost, rmqPort, vHost)
	rmqConn, err := amqp.Dial(rmqURL)
	failIfErr(err)

	if err != nil {
		log.Fatalf("Failed to connect to RabbitMQ: %v", err)
	}

	meanService := &mean.Mean{
		AnalyzerStates: leveldbstate.NewLevelDBState("/home/appuser/state.db"),
		AnalyzerExecutor: &rmqtriggerexecutor.RMQTriggerAnalyzerExecutor{
			JenkinsProject: "Services/Mean/Analyzer_executor_pipeline",
			Publisher:      rabbitmqutil.NewSimplePublisher(rmqConn, EXCHANGE, "analyze-requests"),
		},
		MeanEventStreamer: &rmqeventstreamer.RMQMeanEventStreamer{
			Publisher: rabbitmqutil.NewSimplePublisher(rmqConn, EXCHANGE, "mean-events"),
		},
		AnalyzerEventListener: &rmqeventlistener.RMQAnalyzerEventListener{
			Consumer: rabbitmqutil.NewSimpleConsumer(rmqConn, "svcmean.mean.analyzer-events"),
		},
		MeanRequestListener: &rmqrequestlistener.RMQMeanRequestListener{
			Consumer: rabbitmqutil.NewSimpleConsumer(rmqConn, "svcmean.mean.mean-requests"),
		},
	}

	//meanEventConsumer := rabbitmqutil.NewQueueConsumer(conn, "log_queue")
	meanEventConsumer := rabbitmqutil.NewSimpleConsumer(rmqConn, "svcmean.mean.log-events")
	msgs, err := meanEventConsumer.Consume()
	if err != nil {
		log.Fatal(err)
	}
	go func() {
		for d := range msgs {
			event := &msg.MeanEvent{}
			err := json.Unmarshal(d.Body, event)
			if err != nil {
				log.Fatal(err)
			}
			log.Printf("----------MEAN EVENT----------\n%s\n", string(d.Body))

		}
	}()

	meanService.Start(*configPath)
}

func failIfErr(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}
