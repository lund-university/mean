package main

import (
	"encoding/json"
	"log"
	"os"

	"gitlab.com/lund-university/mean/mean"
	"gitlab.com/lund-university/mean/mean_components/analyzereventlistener/localeventlistener"
	"gitlab.com/lund-university/mean/mean_components/analyzerexecutor/localanalyzerexecutor"
	"gitlab.com/lund-university/mean/mean_components/analyzerstatehandler/leveldbstate"

	msg "gitlab.com/lund-university/mean/message"
)

type meanEventStreamerMock struct{}

func (*meanEventStreamerMock) StreamEvent(meanEvent *msg.MeanEvent) {
	s, err := json.MarshalIndent(meanEvent, "", "    ")
	if err != nil {
		log.Println("Could not marshal meanEvent")
		return
	}
	log.Printf("MEAN EVENT:\n%s\n", s)
}

type meanRequestListenerMock struct{}

func (*meanRequestListenerMock) ListenToMeanRequests(analyzerRequestCallback func(*msg.MeanRequest)) {
	analyzerRequestCallback(
		&msg.MeanRequest{
			Paths:         os.Args[2:],
			SourceContext: msg.SourceContext{"location": os.Args[1]},
		},
	)
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	el := &localeventlistener.LocalEventListener{}
	meanService := &mean.Mean{
		AnalyzerStates: leveldbstate.NewLevelDBState("/tmp/leveldbstate.db"),
		AnalyzerExecutor: &localanalyzerexecutor.LocalAnalyzerExecutor{
			LocalEventListener: el,
		},
		MeanEventStreamer:     &meanEventStreamerMock{},
		AnalyzerEventListener: el,
		MeanRequestListener:   &meanRequestListenerMock{},
	}
	meanService.Start("config/exampleconfigs/global-config-example.yml")
}
