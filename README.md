# MEAN
Meta Analyser


---

## Table of Contents (Optional)

- [Code Contribution Process](#code-contribution-process)
- [Get the source](#get-the-source)
- [Installation](#installation)
- [The Meta-Analyzer Service](#the-mean-service)
- [Analyzer Contribution](#analyzer-contribution)

---

## Code Contribution Process
This is the process for contributing to MEAN:

1. Create a new feature branch.
2. Commit your contribution to that branch.
3. Create a merge request from your feature branch into the master branch.
4. Write a detailed merge description about your contribution, and mention relevant people.
5. Add yourself as an 'assignee' to the merge request.
6. Add a reviewer to share knowledge about what is going on. Also add a relevant expert as reviewer if you need feedback on something you are unsure about. Mention in a merge request comment what you would like the reviewers to focus on and why.
7. Submit the merge request.
8. Merge the request with master after it has been approved.

---

## Get the source
```
git clone https://gitlab.com/lund-university/mean $GOPATH/src/gitlab.com/lund-university/mean
```

---

## Installation

```
cd $GOPATH/src/gitlab.com/lund-university/mean
go install ./...
```

**Note:** MEAN uses [Go Modules](https://github.com/golang/go/wiki/Modules) to manage dependencies.

---

## The Mean Service
> The Meta Analyzer Service

The core of the Mean service is in this repository located in [mean](mean) package.
The service is composed by four communication interfaces and one interface that accounts for analyzers state.
Some implemetations of these interfaces are located in [mean_components](mean_components).
Examples of instances of the Mean service are found in [cmd](cmd).

The Mean service is using a package for configuration located in [config](config).

The Mean service is also using a message protocol implemeted in [message](message).

---

## Analyzer Contribution
An analyzer contributor shall implement the following interface:
- An analyzer is distributed in a docker container
- A volume is expected to be mounted in the container at “/mean”. The structure of this directory is described [here](#directory-structure).
- In the container a directory with all the required source code is located in
“/mean/code/” when container is started - the analyzer container do
not have responsibility for fetching source code.
- The input to be able to run analysis is written to ["/mean/input/analyze_request.json"](#analyze-request).
- The analyzer container is responsible for not listing categories blacklisted by the [analyze request](#analyze-request).
- The analyzer writes its results to the file [“/mean/output/result.json”](#result).
    The result is supposed to be a json-list of json-objects that follow a custom [note protocol](#the-note-protocol).
    Errors that may occur can also be reported in the [result file](#result).
<!-- - Logs are written to stdout and stderr in container -->

### Directory structure
> NOTE: Filenames and directory structure in `code` directory are examples.
```
/mean
├── code
│   ├── example1.py
│   └── example2.py
├── input
│   └── analyze_request.json
└── output
    └── result.json
```
### Analyze Request
```json
{
    "paths": ["example2.py"]
    "blacklisted_categories": ["E0001"]
}
```
### Result
The analyzer docker container is responible for the creation and population of the resultfile. The file consist of "notes" and "errors". Here is an example:
```json
{
    "notes": [
        {
            "location": {
                "path": "example2.py",
                "range": {
                    "start_line": 1
                }
            },
            "description": "This is an error description",
            "category": "E3007"
        },
        {
            "location": {
                "path": "Dockerfile",
                "range": {
                    "start_line": 8,
                    "end_line": 12
                }
            },
            "description": "This is an error description",
            "category": "DL3008"
        },
    ],
    "errors": [
        "Unexpected output from analyzer"
    ]
}
```
#### The Note-protocol
The most ambiguous part is the `range` field. All line count start from 1 and all column counts start from 0. All counts are inclusive. Here is a specification of each field:
- `start_line`
    - If 0 or empty, analyze entire file and ignore fields below
- `start_column`
    - If empty, start column is 0.
    - If set, `start_line` must also be set.
- `end_line`
    - If 0 or empty, then defaults to the same value as start_line.
    - This line is included in the range, i.e. the range spans `end_line-start_line+1` lines.
    - If set, `start_line` must also be set
- `end_column`
    - If 0 or empty, then the position spans until the end of end_line.
    - This column is included in the range, i.e. the range spans `end_column-start_column+1` columns.
    - If set, `start_line` and `end_line` must be set as well.

---
