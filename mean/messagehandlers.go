package mean

import (
	"fmt"
	"log"

	conf "gitlab.com/lund-university/mean/config"
	as "gitlab.com/lund-university/mean/mean/analyzerstate"
	msg "gitlab.com/lund-university/mean/message"
	event "gitlab.com/lund-university/mean/message/event"
)

func (m *Mean) handleAnalyzerEvent(ae *msg.AnalyzerEvent) {
	switch ae.EventType {
	case event.Started:
		err := m.checkState(as.AskedToRun, ae.RequestID, ae.AnalyzerName)
		if err != nil {
			log.Println(err)
			return
		}
		m.SetStatus(ae.RequestID, ae.AnalyzerName, as.Running)

	case event.Result:
		err := m.checkState(as.Running, ae.RequestID, ae.AnalyzerName)
		if err != nil {
			log.Println(err)
			return
		}
		// ae.AnalyzerResult.Notes = m.filterNotes(ae.AnalyzerName, ae.AnalyzerResult.Notes)
		assignIds(ae.AnalyzerResult.Notes)
		m.SetStatus(ae.RequestID, ae.AnalyzerName, as.NotRunning)

	case event.Timeout:
		err := m.checkState(as.Running, ae.RequestID, ae.AnalyzerName)
		if err != nil {
			log.Println(err)
			return
		}
		m.SetStatus(ae.RequestID, ae.AnalyzerName, as.NotRunning)

	case event.Error:
		m.SetStatus(ae.RequestID, ae.AnalyzerName, as.NotRunning)

	default:
		log.Printf("Got an unexpected event: %d ", ae.EventType)
		return
	}

	m.StreamEvent(&msg.MeanEvent{
		EventType:     event.AnalyzerEvent,
		AnalyzerEvent: ae,
	})
}

func (m *Mean) handleMeanRequest(meanReq *msg.MeanRequest) {
	meanReq.RequestID = randomString()
	config, err := conf.MergeConfigs(m.config, meanReq.Config)
	if err != nil {
		m.StreamEvent(&msg.MeanEvent{
			EventType:   event.MeanRequestError,
			MeanRequest: meanReq,
			Info:        err.Error(),
		})
	}

	for analyzerName := range config {
		go func(name string) {
			analyzerConf := config[name]
			m.sendAnalyzeRequest(meanReq, name, &analyzerConf)
		}(analyzerName)
	}
}

func (m *Mean) sendAnalyzeRequest(meanReq *msg.MeanRequest, analyzerName string, analyzerConf *conf.GlobalAnalyzerConfig) {
	switch analyzerConf.Status {
	case conf.Block, conf.Disable:
		m.StreamEvent(&msg.MeanEvent{
			EventType: event.AnalyzerEvent,
			AnalyzerEvent: &msg.AnalyzerEvent{
				RequestID:     meanReq.RequestID,
				AnalyzerName:  analyzerName,
				EventType:     event.NotRelevant,
				SourceContext: meanReq.SourceContext,
				Info: fmt.Sprintf(
					"Analyzer %s did not run. Configured status is `%s`.",
					analyzerName,
					analyzerConf.Status)},
		})
		return
	}

	paths := meanReq.Paths
	if analyzerConf.LocalRegex != "" {
		paths = filterPaths(analyzerConf.LocalRegex, paths)
	}
	paths = filterPaths(analyzerConf.Regex, paths)

	if len(paths) == 0 {
		m.StreamEvent(&msg.MeanEvent{
			EventType: event.AnalyzerEvent,
			AnalyzerEvent: &msg.AnalyzerEvent{
				RequestID:     meanReq.RequestID,
				AnalyzerName:  analyzerName,
				EventType:     event.NotRelevant,
				SourceContext: meanReq.SourceContext,
				Info: fmt.Sprintf(
					"No files match local regex `%s` or global regex `%s`",
					analyzerConf.LocalRegex, analyzerConf.Regex)},
		})
		return
	}

	blacklist := []string{}
	for cat, status := range analyzerConf.Categories {
		switch status {
		case conf.Disable, conf.Block:
			blacklist = append(blacklist, cat)
		}
	}

	analyzeReq := &msg.AnalyzeRequest{
		RequestID:              meanReq.RequestID,
		AnalyzerName:           analyzerName,
		DockerImage:            analyzerConf.Image,
		Timeout: analyzerConf.TimeOut,
		Paths:                  paths,
		SourceContext:          meanReq.SourceContext,
		BlacklistedCategories:  blacklist,
	}

	err := m.checkState(as.NotRunning, analyzeReq.RequestID, analyzeReq.AnalyzerName)
	if err != nil {
		log.Println(err)
		return
	}

	m.SetStatus(analyzeReq.RequestID, analyzeReq.AnalyzerName, as.AskedToRun)
	m.Analyze(analyzeReq)

	m.StreamEvent(&msg.MeanEvent{
		EventType: event.AnalyzerEvent,
		AnalyzerEvent: &msg.AnalyzerEvent{
			RequestID:     analyzeReq.RequestID,
			AnalyzerName:  analyzeReq.AnalyzerName,
			EventType:     event.Scheduled,
			SourceContext: meanReq.SourceContext},
	})
}

func (m *Mean) checkState(expectedState as.AnalyzerState, requestID, analyzerName string) error {
	if state := m.Status(requestID, analyzerName); expectedState != state {
		return fmt.Errorf("Expected status %s, got %s. id=(%s,%s)",
			expectedState, state, requestID, analyzerName)
	}
	return nil

}

func assignIds(notes []*msg.Note) {
	for i := range notes {
		notes[i].NoteId = randomString()
	}
}

func (m *Mean) filterNotes(analyzerName string, notes []*msg.Note) []*msg.Note {
	categories := m.config[analyzerName].Categories //TODO: use the merged config
	var ln int
	for _, note := range notes {
		status := categories[note.Category]
		if status == conf.Disable || status == conf.Block {
			continue
		}
		notes[ln] = note
		ln++
	}
	return notes[:ln]
}
