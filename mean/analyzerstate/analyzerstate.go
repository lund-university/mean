package analyzerstate

import (
	"bytes"
	"encoding/json"
	"fmt"
)

type AnalyzerState uint8

const (
	AskedToRun AnalyzerState = iota + 1
	Running
	NotRunning
)

func (s AnalyzerState) String() string {
	return toString[s]
}

var toString = map[AnalyzerState]string{
	AskedToRun: "asked_to_run",
	Running:    "running",
	NotRunning: "not_running",
}

var toState = inverse(toString)

func inverse(m map[AnalyzerState]string) map[string]AnalyzerState {
	n := make(map[string]AnalyzerState)
	for k, v := range m {
		n[v] = k
	}
	return n
}

// MarshalJSON marshals the enum as a quoted json string
func (s AnalyzerState) MarshalJSON() ([]byte, error) {
	str, ok := toString[s]
	if !ok {
		return nil, fmt.Errorf("Can not marshal AnalyzerState: %d", s)
	}
	buffer := bytes.NewBufferString(`"`)
	buffer.WriteString(str)
	buffer.WriteString(`"`)
	return buffer.Bytes(), nil
}

// UnmarshalJSON unmashals a quoted json string to the enum value
func (s *AnalyzerState) UnmarshalJSON(b []byte) error {
	var str string
	err := json.Unmarshal(b, &str)
	if err != nil {
		return err
	}
	event, ok := toState[str]
	if !ok {
		return fmt.Errorf("Can not unmarshal \"%s\" to AnalyzerEvent", str)
	}
	*s = event
	return nil
}
