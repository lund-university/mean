/*
Package mean implements the core logic of a meta analyzer service.

It consist of a struct Mean composed of a few interfaces which are to
be implemented in other packages.
*/
package mean

import (
	conf "gitlab.com/lund-university/mean/config"
	as "gitlab.com/lund-university/mean/mean/analyzerstate"
	msg "gitlab.com/lund-university/mean/message"
)

type (
	// AnalyzerExecutor is responsible for starting analyzers.
	// Analyzer events are to be reported back to the AnalyzerEventListener
	AnalyzerExecutor interface {
		Analyze(*msg.AnalyzeRequest)
	}

	// MeanEventStreamer is responsibile for reporting events from this
	// service to a for the application appropriate place.
	MeanEventStreamer interface {
		StreamEvent(*msg.MeanEvent)
	}

	// AnalyzerEventListener is responsible for reporting incoming analyzer events
	// using callback function. The callback is called in its
	// own goroutine.
	AnalyzerEventListener interface {
		ListenToAnalyzerEvents(func(*msg.AnalyzerEvent))
	}

	// MeanRequestListener is responsible for reporting incoming mean requests
	// by ing callback function. The callback is called in its
	// own goroutine.
	MeanRequestListener interface {
		ListenToMeanRequests(func(*msg.MeanRequest))
	}

	// AnalyzerStates is responsible for keeping state of running analyzers.
	AnalyzerStates interface {
		SetStatus(reqID string, analyzerName string, status as.AnalyzerState)
		Status(reqID string, analyzerName string) as.AnalyzerState
		Statuses(reqID string) map[string]as.AnalyzerState
		AllStatuses() map[string]map[string]as.AnalyzerState
	}

	// Mean struct is composed of the 5 interfaces above and a global configuration.
	Mean struct {
		AnalyzerStates
		AnalyzerExecutor
		MeanEventStreamer
		AnalyzerEventListener
		MeanRequestListener
		config conf.GlobalConfig
	}
)

// Start runs the Mean service.
func (m *Mean) Start(configPath string) {
	m.config = conf.LoadConfig(configPath)

	go m.ListenToAnalyzerEvents(
		func(analyzerEvent *msg.AnalyzerEvent) {
			m.handleAnalyzerEvent(analyzerEvent)
		},
	)

	go m.ListenToMeanRequests(
		func(meanRequest *msg.MeanRequest) {
			go m.handleMeanRequest(meanRequest)
		},
	)

	select {}
}
