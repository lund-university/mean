package mean

import (
	"log"
	"math/rand"
	"regexp"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func filterPaths(regexString string, paths []string) (filesWithExt []string) {
	regex, err := regexp.Compile(regexString)
	if err != nil {
		log.Printf("Did not filter paths. Regex error: %v", err)
		return []string{}
	}

	for _, path := range paths {
		if regex.MatchString(path) {
			filesWithExt = append(filesWithExt, path)
		}
	}
	return filesWithExt
}

func randomString() string {
	const (
		charset       = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
		lenCharset    = len(charset)
		lenRandString = 16
	)
	b := make([]byte, lenRandString)
	for i := range b {
		b[i] = charset[rand.Intn(lenCharset)]
	}
	return string(b)
}
