package rmqtriggerexecutor

import (
	"encoding/json"
	"log"

	"github.com/streadway/amqp"
	rmq "gitlab.com/lund-university/mean/mean_components/util/rabbitmqutil"
	msg "gitlab.com/lund-university/mean/message"
)

type RMQTriggerAnalyzerExecutor struct {
	JenkinsProject string
	rmq.Publisher
}

type RabbitMQMessage struct {
	Project   string      `json:"project"`
	Token     string      `json:"token"`
	Parameter []Parameter `json:"parameter"`
}

type Parameter struct {
	Name  string              `json:"name"`
	Value *msg.AnalyzeRequest `json:"value"`
}

func (a *RMQTriggerAnalyzerExecutor) Analyze(analyzeReq *msg.AnalyzeRequest) {
	rabbitMQMessage := RabbitMQMessage{
		Project: a.JenkinsProject, //"AnalyzerExecutor", Change this
		Token:   "AESToken",
		Parameter: []Parameter{
			Parameter{
				Name:  "AnalyzeRequest",
				Value: analyzeReq,
			},
		},
	}

	reqBytes, err := json.Marshal(rabbitMQMessage)

	if err != nil {
		log.Printf("Could not unmarshal analyzeRequest: %s", err)
		return
	}

	err = a.Publish(&amqp.Publishing{
		ContentType: "application/json",
		AppId:       "remote-build",
		Body:        reqBytes,
	})

	if err != nil {
		log.Printf("Could not unmarshal analyzeRequest: %s", err)
	}
}
