package localanalyzerexecutor

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"os/exec"

	msg "gitlab.com/lund-university/mean/message"
	event "gitlab.com/lund-university/mean/message/event"
)

const dockerImage = "mean_pylint"

type LocalEventListener interface {
	Report(*msg.AnalyzerEvent)
}

type LocalAnalyzerExecutor struct {
	LocalEventListener LocalEventListener
}

func (ae *LocalAnalyzerExecutor) Analyze(analyzeReq *msg.AnalyzeRequest) {
	// Suppose to be remote procedure, so start thread
	go func() {
		meanDir, err := ioutil.TempDir("", "mean")
		if err != nil {
			log.Fatal(err)
		}
		defer os.RemoveAll(meanDir)
		inputDir := meanDir + "/input"
		outputDir := meanDir + "/output"
		codeDir := analyzeReq.SourceContext["location"].(string)

		if err := os.MkdirAll(inputDir, os.ModePerm); err != nil {
			log.Printf("Could not create dir %s: %v", inputDir, err)
			return
		}
		if err := os.MkdirAll(outputDir, os.ModePerm); err != nil {
			log.Printf("Could not create dir %s: %v", meanDir+"/output", err)
			return
		}
		data, err := json.Marshal(analyzeReq)
		if err != nil {
			log.Fatal(err)
		}
		err = ioutil.WriteFile(inputDir+"/analyze_request.json", data, 0644)
		if err != nil {
			log.Fatal(err)
		}

		ae.LocalEventListener.Report(
			&msg.AnalyzerEvent{
				RequestID:    analyzeReq.RequestID,
				AnalyzerName: analyzeReq.AnalyzerName,
				EventType:    event.Started,
			},
		)

		cmd := exec.Command("docker",
			"run",
			"-v", inputDir+":/mean/input:ro",
			"-v", outputDir+":/mean/output",
			"-v", codeDir+":/mean/code:ro",
			analyzeReq.DockerImage)
		buf, err := cmd.CombinedOutput()
		if err != nil {
			log.Printf("Error running docker image %s: %v\n", dockerImage, err)
			log.Printf("Buf: %s\n", string(buf))
			return
		}
		dat, err := ioutil.ReadFile(outputDir + "/result.json")
		if err != nil {
			log.Printf("Could not read result.json: %v", err)
		}

		// send result event to localEventListener
		result := &msg.AnalyzerResult{}
		json.Unmarshal(dat, result)

		event := &msg.AnalyzerEvent{
			RequestID:      analyzeReq.RequestID,
			AnalyzerName:   analyzeReq.AnalyzerName,
			EventType:      event.Result,
			AnalyzerResult: result,
		}

		ae.LocalEventListener.Report(event)
	}()
}
