package leveldbstate

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"

	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/util"
	as "gitlab.com/lund-university/mean/mean/analyzerstate"
)

type levelDBState struct {
	db *leveldb.DB
}

func NewLevelDBState(dbPath string) levelDBState {
	db, err := leveldb.OpenFile(dbPath, nil)
	if err != nil {
		log.Fatal(err)
	}
	return levelDBState{db}
}

func (s levelDBState) SetStatus(reqID string, analyzerID string, status as.AnalyzerState) {
	statusdata, _ := status.MarshalJSON()
	key := makeKey(reqID, analyzerID)
	if status == as.NotRunning {
		s.db.Delete(key, nil)
	} else {
		s.db.Put(key, statusdata, nil)
	}
}

func makeKey(reqID, analyzerID string) []byte {
	return []byte(reqID + ":" + analyzerID)
}

func splitKey(key []byte) (string, string) {
	strs := strings.SplitN(string(key), ":", 2)
	if len(strs) != 2 {
		return "", ""
	}
	return strs[0], strs[1]
}

func (s levelDBState) Status(reqID string, analyzerID string) as.AnalyzerState {
	data, err := s.db.Get(makeKey(reqID, analyzerID), nil)
	if err != nil {
		switch err {
		case leveldb.ErrNotFound:
			return as.NotRunning
		}
	}
	var status as.AnalyzerState
	status.UnmarshalJSON(data)
	return status
}

func (s levelDBState) Statuses(reqID string) map[string]as.AnalyzerState {
	states := map[string]as.AnalyzerState{}

	iter := s.db.NewIterator(util.BytesPrefix([]byte(reqID)), nil)

	for iter.Next() {
		_, anayzerName := splitKey(iter.Key())
		var state as.AnalyzerState
		err := json.Unmarshal(iter.Value(), &state)
		if err == nil {
			states[anayzerName] = state
		}
	}
	iter.Release()
	// err := iter.Error()
	return states
}

func (s levelDBState) AllStatuses() map[string]map[string]as.AnalyzerState {
	states := map[string]map[string]as.AnalyzerState{}

	iter := s.db.NewIterator(nil, nil)
	for iter.Next() {
		reqID, analyzerName := splitKey(iter.Key())
		var state as.AnalyzerState
		err := json.Unmarshal(iter.Value(), &state)
		if err == nil {
			if _, ok := states[reqID]; !ok {
				states[reqID] = map[string]as.AnalyzerState{}
			}
			states[reqID][analyzerName] = state
		}
	}
	iter.Release()
	return states
}

func (s levelDBState) PrintState() {
	iter := s.db.NewIterator(nil, nil)
	for iter.Next() {
		key := iter.Key()
		value := iter.Value()
		fmt.Printf("%s -> %s\n", key, value)
	}
}
