package analyzerstatemap

import (
	"encoding/json"
	"fmt"
	"testing"

	as "gitlab.com/lund-university/mean/mean/analyzerstate"
)

func TestStatusAndSetstatus(t *testing.T) {
	sm := NewAnalyzerStateMap()

	// set
	sm.SetStatus("1234", "pylint", as.AskedToRun)
	if s := sm.Status("1234", "pylint"); s != as.AskedToRun {
		t.Errorf("Wanted: %s. Got %s", as.AskedToRun, s)
	}

	// remove when NotRunning
	sm.SetStatus("1234", "pylint", as.NotRunning)
	if _, ok := sm.stateMap[key{"1234", "pylint"}]; ok {
		t.Errorf("Should have been removed when set %s", as.NotRunning)
	}

	// return NotRunning when does not exist
	if s := sm.Status("1234", "pylint"); s != as.NotRunning {
		t.Errorf("Was %s. Should be %s", s, as.NotRunning)
	}
}

func TestJson(t *testing.T) {
	sm := NewAnalyzerStateMap()
	// set
	sm.SetStatus("req1", "pylint", as.AskedToRun)
	bytes, err := json.Marshal(sm)
	if err != nil {
		t.Error(err)
	}
	fmt.Println(string(bytes))

	err = json.Unmarshal([]byte("[]"), sm)
	if err != nil {
		t.Error(err)
	}
	if len(sm.stateMap) != 0 {
		t.Error("Len should be 0")
	}
	fmt.Printf("%+v\n", sm.stateMap)

	err = json.Unmarshal(bytes, sm)
	if err != nil {
		t.Error(err)
	}
	fmt.Printf("%+v\n", sm.stateMap)

}
