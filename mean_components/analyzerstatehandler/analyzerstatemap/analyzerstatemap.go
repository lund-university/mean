package analyzerstatemap

import (
	"encoding/json"
	"sync"

	as "gitlab.com/lund-university/mean/mean/analyzerstate"
)

type (
	key struct {
		ReqID      string `json:"req_id"`
		AnalyzerID string `json:"analyzer_id"`
	}

	AnalyzerStateMap struct {
		stateMap map[key]as.AnalyzerState
		mux      sync.RWMutex
	}

	keyValue struct {
		Key   key              `json:"key"`
		Value as.AnalyzerState `json:"value"`
	}
)

// var _ encoding.TextMarshaler = key{}
// var _ encoding.TextUnmarshaler = &key{}

func NewAnalyzerStateMap() *AnalyzerStateMap {
	return &AnalyzerStateMap{stateMap: make(map[key]as.AnalyzerState)}
}

func (s *AnalyzerStateMap) SetStatus(reqID string, analyzerID string, status as.AnalyzerState) {
	k := key{reqID, analyzerID}
	if status == as.NotRunning {
		s.remove(&k)
	} else {
		s.set(&k, status)
	}
}

func (s *AnalyzerStateMap) Status(reqID string, analyzerID string) as.AnalyzerState {
	k := key{reqID, analyzerID}
	state, ok := s.get(&k)
	if !ok {
		state = as.NotRunning
	}
	return state
}

func (s *AnalyzerStateMap) remove(k *key) {
	s.mux.Lock()
	defer s.mux.Unlock()
	delete(s.stateMap, *k)
}

func (s *AnalyzerStateMap) set(k *key, state as.AnalyzerState) {
	s.mux.Lock()
	defer s.mux.Unlock()
	s.stateMap[*k] = state
}

func (s *AnalyzerStateMap) get(k *key) (as.AnalyzerState, bool) {
	s.mux.RLock()
	defer s.mux.RUnlock()
	state, ok := s.stateMap[*k]
	return state, ok
}

func (s *AnalyzerStateMap) MarshalJSON() ([]byte, error) {
	kvList := make([]keyValue, 0, len(s.stateMap))

	s.mux.RLock()
	for k, v := range s.stateMap {
		kvList = append(kvList, keyValue{k, v})
	}
	s.mux.RUnlock()
	return json.Marshal(kvList)
}

func (s *AnalyzerStateMap) UnmarshalJSON(data []byte) error {
	var kvList []keyValue
	if err := json.Unmarshal(data, &kvList); err != nil {
		return err
	}
	newStateMap := make(map[key]as.AnalyzerState, len(kvList))
	for _, kv := range kvList {
		newStateMap[kv.Key] = kv.Value
	}
	s.mux.Lock()
	s.stateMap = newStateMap
	s.mux.Unlock()
	return nil
}
