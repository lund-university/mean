package localeventlistener

import (
	msg "gitlab.com/lund-university/mean/message"
)

type LocalEventListener struct {
	reportToMean func(*msg.AnalyzerEvent)
}

func (el *LocalEventListener) Report(event *msg.AnalyzerEvent) {
	el.reportToMean(event)
}

func (el *LocalEventListener) ListenToAnalyzerEvents(
	reportToMean func(*msg.AnalyzerEvent)) {
	el.reportToMean = reportToMean
}
