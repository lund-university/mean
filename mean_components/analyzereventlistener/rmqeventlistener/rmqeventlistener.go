package rmqeventlistener

import (
	"encoding/json"
	"log"

	rmq "gitlab.com/lund-university/mean/mean_components/util/rabbitmqutil"
	msg "gitlab.com/lund-university/mean/message"
)

type RMQAnalyzerEventListener struct {
	rmq.Consumer
}

func (a *RMQAnalyzerEventListener) ListenToAnalyzerEvents(reportToMean func(*msg.AnalyzerEvent)) {
	msgs, err := a.Consume()
	failOnError(err, "Failed to register a consumer")

	go func() {
		for d := range msgs {
			event := &msg.AnalyzerEvent{}
			if err := json.Unmarshal(d.Body, event); err != nil {
				log.Printf("Failed to unmarshal AnalyzerEvent %s: %e", string(d.Body), err)
			}
			reportToMean(event)
		}
	}()

	select {}
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}
