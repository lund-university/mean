package rmqrequestlistener

import (
	"encoding/json"
	"log"

	rmq "gitlab.com/lund-university/mean/mean_components/util/rabbitmqutil"
	msg "gitlab.com/lund-university/mean/message"
)

type RMQMeanRequestListener struct {
	rmq.Consumer
}

func (a *RMQMeanRequestListener) ListenToMeanRequests(reportToMean func(*msg.MeanRequest)) {
	msgs, err := a.Consume()
	failOnError(err, "Failed to register a consumer")

	go func() {
		for d := range msgs {
			req := &msg.MeanRequest{}
			if err := json.Unmarshal(d.Body, req); err != nil {
				log.Printf("Failed to unmarshal MeanRequest %s: %v", string(d.Body), err)
			}
			reportToMean(req)
		}
	}()

	select {}
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}
