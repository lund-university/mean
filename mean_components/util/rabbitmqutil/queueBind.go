package rabbitmqutil

import (
	"fmt"

	"github.com/streadway/amqp"
)

type QueueBindDecl map[string]map[string]string

func BindQueues(conn *amqp.Connection, exchangeToQueueToRoutingKey QueueBindDecl) error {
	ch, err := conn.Channel()
	if err != nil {
		return fmt.Errorf("Could not create channel: %v", err)
	}
	defer ch.Close()

	for exchange, queueToTopic := range exchangeToQueueToRoutingKey {
		err = ch.ExchangeDeclare(
			exchange, // name
			"topic",  // type
			true,     // durable
			false,    // auto-deleted
			false,    // internal
			false,    // no-wait
			nil,      // arguments
		)
		if err != nil {
			return fmt.Errorf("Could not declare exchange: %v", err)
		}
		for queue, routingKey := range queueToTopic {
			_, err := ch.QueueDeclare(
				queue, // name
				false, // durable
				false, // delete when unused
				false, // exclusive
				false, // no-wait
				nil,   // arguments
			)
			if err != nil {
				return fmt.Errorf("Could not declare queue: %v", err)
			}

			err = ch.QueueBind(
				queue,      // queue name
				routingKey, // routing key
				exchange,   // exchange
				false,
				nil,
			)

			if err != nil {
				return fmt.Errorf("Could not bind queue: %v", err)
			}

		}
	}
	return nil
}
