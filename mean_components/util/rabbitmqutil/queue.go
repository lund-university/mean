package rabbitmqutil

import (
	"fmt"

	"github.com/streadway/amqp"
)

type queue struct {
	conn  *amqp.Connection
	queue amqp.Queue
}

func NewQueueConsumer(conn *amqp.Connection, queueName string) Consumer {
	return newQueue(conn, queueName)
}

func NewQueuePublisher(conn *amqp.Connection, queueName string) Publisher {
	return newQueue(conn, queueName)
}

func newQueue(conn *amqp.Connection, queueName string) *queue {
	a := &queue{conn: conn}

	ch, err := a.conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	a.queue, err = ch.QueueDeclare(
		queueName, // name
		false,     // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)
	failOnError(err, "Failed to declare a queue")

	return a
}

func (q *queue) Consume() (<-chan amqp.Delivery, error) {
	msgs := make(chan amqp.Delivery)

	ch, err := q.conn.Channel()
	if err != nil {
		return msgs, err
	}

	_msgs, err := ch.Consume(
		q.queue.Name, // queue
		"",           // consumer
		true,         // auto-ack
		false,        // exclusive
		false,        // no-local
		false,        // no-wait
		nil,          // args
	)
	if err != nil {
		return msgs, err
	}

	go func() {
		for msg := range _msgs {
			msgs <- msg
		}
		close(msgs)
		ch.Close()
	}()

	return msgs, err
}

func (q *queue) Publish(msg *amqp.Publishing) error {
	ch, err := q.conn.Channel()
	if err != nil {
		return fmt.Errorf("Failed to open a channel: %v", err)
	}
	defer ch.Close()

	err = ch.Publish(
		"",           // exchange
		q.queue.Name, // routing key
		false,        // mandatory
		false,        // immediate
		*msg,
	)
	if err != nil {
		return fmt.Errorf("Failed to publish a message: %v", err)
	}
	return nil
}
