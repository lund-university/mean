package rabbitmqutil

import (
	"fmt"

	"github.com/streadway/amqp"
)

type simplePublisher struct {
	conn       *amqp.Connection
	exchange   string
	routingKey string
}

func NewSimplePublisher(conn *amqp.Connection, exchange string, routingKey string) Publisher {
	return &simplePublisher{
		conn:       conn,
		exchange:   exchange,
		routingKey: routingKey,
	}
}

func (publisher *simplePublisher) Publish(msg *amqp.Publishing) error {
	ch, err := publisher.conn.Channel()
	if err != nil {
		return fmt.Errorf("Failed to open a channel: %v", err)
	}
	defer ch.Close()

	err = ch.Publish(
		publisher.exchange,   // exchange
		publisher.routingKey, // routing key
		false,                // mandatory
		false,                // immediate
		*msg,
	)
	if err != nil {
		return fmt.Errorf("Failed to publish a message: %v", err)
	}
	return nil
}
