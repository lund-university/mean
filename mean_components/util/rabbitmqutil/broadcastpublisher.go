package rabbitmqutil

import (
	"fmt"

	"github.com/streadway/amqp"
)

type broadcastPublisher struct {
	conn     *amqp.Connection
	exchange string
	queues   []amqp.Queue
}

func NewBroadcastPublisher(conn *amqp.Connection, exchange string, queueNames []string) Publisher {
	publisher := &broadcastPublisher{conn: conn, exchange: exchange}

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	err = ch.ExchangeDeclare(
		publisher.exchange, // name
		"fanout",           // type
		true,               // durable
		false,              // auto-deleted
		false,              // internal
		false,              // no-wait
		nil,                // arguments
	)
	failOnError(err, "Failed to declare exchange")

	for _, qName := range queueNames {
		q, err := ch.QueueDeclare(
			qName, // name
			false, // durable
			false, // delete when unused
			false, // exclusive
			false, // no-wait
			nil,   // arguments
		)
		failOnError(err, "Failed to declare a queue")
		publisher.queues = append(publisher.queues, q)

		err = ch.QueueBind(
			qName,              // queue name
			"",                 // routing key
			publisher.exchange, // exchange
			false,
			nil,
		)
		failOnError(err, "Failed to declare exchange")
	}

	return publisher
}

func (this *broadcastPublisher) Publish(msg *amqp.Publishing) error {
	ch, err := this.conn.Channel()
	if err != nil {
		return fmt.Errorf("Failed to open a channel: %v", err)
	}
	defer ch.Close()

	err = ch.Publish(
		this.exchange, // exchange
		"",            // routing key
		false,         // mandatory
		false,         // immediate
		*msg,
	)
	if err != nil {
		return fmt.Errorf("Failed to publish a message: %v", err)
	}
	return nil
}
