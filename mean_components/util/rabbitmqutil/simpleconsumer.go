package rabbitmqutil

import (
	"github.com/streadway/amqp"
)

type simpleConsumer struct {
	conn      *amqp.Connection
	queueName string
}

func NewSimpleConsumer(conn *amqp.Connection, queueName string) Consumer {
	return &simpleConsumer{
		conn:      conn,
		queueName: queueName,
	}
}

func (consumer *simpleConsumer) Consume() (<-chan amqp.Delivery, error) {
	msgs := make(chan amqp.Delivery)

	ch, err := consumer.conn.Channel()
	if err != nil {
		return msgs, err
	}

	_msgs, err := ch.Consume(
		consumer.queueName, // queue
		"",                 // consumer
		true,               // auto-ack
		true,               // exclusive
		false,              // no-local
		false,              // no-wait
		nil,                // args
	)
	if err != nil {
		return msgs, err
	}

	go func() {
		for msg := range _msgs {
			msgs <- msg
		}
		close(msgs)
		ch.Close()
	}()

	return msgs, err
}
