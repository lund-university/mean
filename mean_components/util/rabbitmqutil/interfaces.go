package rabbitmqutil

import (
	"log"

	"github.com/streadway/amqp"
)

type (
	Consumer interface {
		Consume() (<-chan amqp.Delivery, error)
	}
	Publisher interface {
		Publish(*amqp.Publishing) error
	}
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}
