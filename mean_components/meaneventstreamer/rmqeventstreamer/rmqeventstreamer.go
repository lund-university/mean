package rmqeventstreamer

import (
	"encoding/json"
	"log"

	"github.com/streadway/amqp"
	rmq "gitlab.com/lund-university/mean/mean_components/util/rabbitmqutil"
	msg "gitlab.com/lund-university/mean/message"
)

type RMQMeanEventStreamer struct {
	rmq.Publisher
	// notepub rmq.Publisher
}

func (a *RMQMeanEventStreamer) StreamEvent(event *msg.MeanEvent) {
	eventBytes, err := json.Marshal(event)
	if err != nil {
		log.Printf("Could not marshal event: %s", err)
		return
	}

	err = a.Publish(&amqp.Publishing{
		ContentType: "application/json",
		Body:        eventBytes,
	})

	if err != nil {
		log.Printf("Could not publish: %s", err)
	}

	// // Hopefully searchable note format
	// if event.EventType == eventtype.AnalyzerEvent &&
	// 	event.AnalyzerEvent != nil &&
	// 	event.AnalyzerEvent.EventType == eventtype.Result {

	// 	for _, note := range event.AnalyzerEvent.AnalyzerResult.Notes {
	// 		wrappednote := struct {
	// 			AnalyzerName string
	// 			RequestID    string
	// 			Note         *msg.Note
	// 		}{
	// 			AnalyzerName: event.AnalyzerEvent.AnalyzerName,
	// 			RequestID:    event.AnalyzerEvent.RequestID,
	// 			Note:         note,
	// 		}
	// 		eventBytes, err := json.Marshal(wrappednote)
	// 		logOnError(err, "Could not marshal event")
	// 		if err != nil {
	// 			continue
	// 		}
	// 		err = a.notepub.Publish(&amqp.Publishing{
	// 			ContentType: "application/json",
	// 			Body:        eventBytes,
	// 		})
	// 	}
	// }

}
