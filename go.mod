module gitlab.com/lund-university/mean

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/streadway/amqp v1.0.0
	github.com/syndtr/goleveldb v1.0.0
	go.mongodb.org/mongo-driver v1.5.1 // indirect
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
