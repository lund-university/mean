import sys
import pika
import ssl

def publish_event(channel, exchange, routing_key, event):
    channel.basic_publish(exchange=exchange,
                          routing_key=routing_key,
                          body=event)
    print('SENT [x] %r: "%r"' % (routing_key, event))


if __name__ == "__main__":

    if len(sys.argv) == 5:
        host = sys.argv[1]
        exchange = sys.argv[2]
        routing_key = sys.argv[3]
        filename = sys.argv[4]
        # TODO: Fix duplicated code
        parameters = pika.ConnectionParameters(host=host,
                                                port=5672)
    elif len(sys.argv) == 7:
        host = sys.argv[1]
        exchange = sys.argv[2]
        routing_key = sys.argv[3]
        filename = sys.argv[4]
        username = sys.argv[5]
        password = sys.argv[6]
        credentials = pika.PlainCredentials(username, password)
        ssl_options = pika.SSLOptions(context=ssl.SSLContext(ssl.PROTOCOL_TLSv1))
        parameters = pika.ConnectionParameters(host=host,
                                               port=5671,
                                               ssl_options=ssl_options,
                                               credentials=credentials)
    else:
        print(len(sys.argv), "is not correct number of arguments!")
        sys.exit(1)

    with open(filename, 'r') as f:
        body = f.read()


    with pika.BlockingConnection(parameters) as rmq_conn:
        channel = rmq_conn.channel()
        publish_event(channel, exchange, routing_key, body)
        channel.close()
