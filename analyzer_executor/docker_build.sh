#!/bin/sh
docker build -t rabbitmq_publisher -f Dockerfile .

while true; do
    read -p "Do you wish to push to artifactory? " yn
    case $yn in
        [Yy]* )
            docker tag rabbitmq_publisher /* replace */
            docker push /* replace */
            break
            ;;
        [Nn]* ) break;;
        * ) echo "Please answer yes or no.";;
    esac
done