#!/usr/bin/env python3
import json
import os
import pika
import shlex
import sys
import math
import collections
from subprocess import Popen, PIPE, TimeoutExpired
#import shutil


HOST = 'localhost'
REQUEST_FILENAME = 'analyze_request.json'
RESULT_FILENAME = 'result.json'
QUEUE_NAME = 'analyzer_events'
BASE_DIR = 'mean'
INPUT_DIR = BASE_DIR + '/input'
OUTPUT_DIR = BASE_DIR + '/output'
CODE_DIR = BASE_DIR + '/code'

Result = collections.namedtuple('Result', 'stdout stderr returncode')

def run(cmd, timeout=None):
    proc = Popen(cmd,
                 stdout = PIPE,
                 stderr = PIPE
    )
    try:
        stdout, stderr = proc.communicate(timeout=timeout)
    except TimeoutExpired:
        proc.kill()
        raise
    return Result(stdout=stdout.decode("utf-8"),
                  stderr=stderr.decode("utf-8"),
                  returncode=proc.returncode)

def create_mean_dir(analyzer_request):
    remove_mean_dir()
    for dir in BASE_DIR, INPUT_DIR, CODE_DIR:
        os.mkdir(dir)

    with open(INPUT_DIR + '/' + REQUEST_FILENAME, "w+") as f:
        f.write(json.dumps(analyzer_request))

def remove_mean_dir():
    os.system('sudo rm -rf ' + BASE_DIR)
    #shutil.rmtree(BASE_DIR)

def pull_code(source_context):
    host_uri = source_context.get('host_uri')
    project_name = source_context.get('project_name')
    ref = source_context.get('ref')
    if not(host_uri and project_name and ref):
        return False

    ret_code = os.system('git clone "http://' +
                         host_uri +
                         '/' +
                         project_name +
                         '.git" ' +
                         CODE_DIR)
    if ret_code != 0:
        return False
    cwd = os.getcwd()
    os.chdir(cwd + '/' + CODE_DIR)
    ret_code = os.system('git fetch "http://' +
                         host_uri +
                         '/' +
                         project_name +
                         '.git" ' +
                         ref +
                         ' && git checkout FETCH_HEAD')
    os.chdir(cwd)
    if ret_code != 0:
        return False
    return True

def start_analyzer(analyzer_image, analyzer_name, analyzer_timeout):
    mnt_src = os.getcwd() + '/' + BASE_DIR
    mnt_target = '/' + BASE_DIR
    cmd = ['docker', 'run',
           '-v', mnt_src + ':' + mnt_target,
           analyzer_image]

    try:
        result = run(cmd, analyzer_timeout)
    except TimeoutExpired:
        return 'timeout', ''
    if result.returncode != 0:
        return 'error', result.stderr
    return 'result', ''

def publish_event(channel, event):
    channel.basic_publish(exchange='',
                          routing_key=QUEUE_NAME,
                          body=json.dumps(event))
    print('SENT [x] %r: "%r"' % (QUEUE_NAME, event))


def create_analyzer_event(request_id,
                          analyzer_name,
                          event_type,
                          source_context,
                          info=''):
    analyzer_event = {'request_id': request_id,
                      'analyzer_name': analyzer_name,
                      'event_type': event_type,
                      'source_context': source_context}

    if info != '':
        analyzer_event['info'] = info
    if event_type == 'result':
        if os.path.exists(OUTPUT_DIR + '/' + RESULT_FILENAME):
            with open(OUTPUT_DIR + '/' + RESULT_FILENAME, 'r') as f:
                analyzer_event['analyzer_result'] = json.loads(f.read())
        else:
            analyzer_event['event_type'] = 'error'
            analyzer_event['info'] = 'no result file found, check that the analyzer finished correctly'
    return analyzer_event

if __name__ == "__main__":
    req_str = os.getenv('AnalyzeRequest')
    if req_str == None:
        print("AnalyzeRequest env var is undefined", file=sys.stderr)
        sys.exit(1)

    analyzer_request = json.loads(req_str)
    request_id = analyzer_request['request_id']
    source_context = analyzer_request['source_context']
    analyzer_image = analyzer_request['docker_image']
    analyzer_name = analyzer_request['analyzer_name']
    analyzer_timeout = 60*analyzer_request.get('timeout', math.inf)

    with pika.BlockingConnection(pika.ConnectionParameters(host=HOST)) as rmq_conn:
        channel = rmq_conn.channel()
        channel.queue_declare(queue=QUEUE_NAME)
        analyzer_started = create_analyzer_event(request_id,
                                                 analyzer_name,
                                                 'started',
                                                 source_context)
        publish_event(channel, analyzer_started)

        try:
            create_mean_dir(analyzer_request)
        except OSError:
            analyzer_error = create_analyzer_event(request_id,
                                                   analyzer_name,
                                                   'error',
                                                   source_context,
                                                   'Could not create mean directory')
            publish_event(channel, analyzer_error)
            sys.exit(1)
        if not pull_code(source_context):
            analyzer_error = create_analyzer_event(request_id,
                                                   analyzer_name,
                                                   'error',
                                                   source_context,
                                                   'Could not pull code')
            publish_event(channel, analyzer_error)
            print("Could not pull code", file=sys.stderr)

        event_type, info = start_analyzer(analyzer_image,
                                          analyzer_name,
                                          analyzer_timeout)

        analyzer_event = create_analyzer_event(request_id,
                                               analyzer_name,
                                               event_type,
                                               source_context,
                                               info)
        publish_event(channel, analyzer_event)
        channel.close()
        remove_mean_dir()

    sys.exit(0 if event_type=='result' else 1)
