#!/usr/bin/env python3
import json
import os
import pika
from urllib.parse import urlparse
import yaml
import sys
import ssl
import requests

URL = /* replace */
QUEUE_NAME = 'mean-requests'
HOST = /* replace */
PORT = /* replace */
EXCHANGE = /* replace */

def publish_analyze_request(connection, message):
    channel = connection.channel()
    channel.basic_publish(exchange=EXCHANGE,
                          routing_key=QUEUE_NAME,
                          body=message)
    print('SENT [x] %r: "%r"' % (QUEUE_NAME, message))
    channel.close()

def get_updated_files(change_id, revision_id):
    url = URL + '/changes/' + change_id + '/?o=ALL_REVISIONS&o=ALL_FILES'
    content = requests.get(url, auth=(sys.argv[1], sys.argv[2])).content
    change_info = json.loads(str(content, 'utf-8').lstrip(")]}'"))
    return list(change_info['revisions'][revision_id]['files'].keys())

def create_mean_request(project_name, host_uri, ref, paths, change_id, revision_id, config):
    source_context = {'host_uri': host_uri, 'project_name': project_name, 'ref': ref, 'change_id': change_id, 'revision_id': revision_id}
    mean_request = {'paths': paths, 'source_context': source_context}
    if config != None:
        mean_request['config'] = config
    return json.dumps(mean_request)

def get_local_config(url, project_name):
    print(url + '/' + project_name)
    temp = url.split('://', 1)
    print('git clone "' + temp[0] + '://' + 'username' + ':' + 'password' + '@' + temp[1] + '/a/' + project_name + '.git"')
    os.system('git clone "' + temp[0] + '://' + temp[1] + '/' + project_name + '.git"')
    cur_dir = os.getcwd()
    git_dir = project_name.split('/')[-1]
    os.chdir(os.path.join(cur_dir, git_dir))
    print(os.getcwd())
    os.system('git checkout meta/config')
    if not os.path.exists('meanConfig.yaml'):
        os.chdir(cur_dir)
        os.system('rm -rf ' + git_dir)
        print('File does not exists!!!!')
        return None
    print(open('meanConfig.yaml', 'r').read())
    with open('meanConfig.yaml', 'r') as f:
        try:
            config = yaml.safe_load(f)
            print(config)
        except yaml.YAMLError as exc:
            print(exc)
    os.chdir(cur_dir)
    os.system('rm -rf ' + git_dir)
    return config


if __name__ == "__main__":
    change_id = os.getenv('GERRIT_CHANGE_ID')
    revision_id = os.getenv('GERRIT_PATCHSET_REVISION')
    project_name = os.getenv('GERRIT_PROJECT')
    ref = os.getenv('GERRIT_REFSPEC')

    host_uri = URL
    paths = get_updated_files(change_id, revision_id)
    config = get_local_config(host_uri, project_name)

    mean_request = create_mean_request(project_name, host_uri, ref, paths, change_id, revision_id, config)

    if len(sys.argv) == 3:
        user = sys.argv[1]
        password = sys.argv[2]
        credentials = pika.PlainCredentials(user, password)
        ssl_options = pika.SSLOptions(context=ssl.SSLContext(ssl.PROTOCOL_TLSv1))
        parameters = pika.ConnectionParameters(host=HOST,
                                            port=PORT,
                                            ssl_options=ssl_options,
                                            credentials=credentials)
    else:
        parameters = pika.ConnectionParameters(host="localhost",
                                            port=5672)
    with pika.BlockingConnection(parameters) as publish_conn:
        publish_analyze_request(publish_conn, mean_request)
