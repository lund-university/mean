#!/usr/bin/env python3
import json
import os
import sys
import ssl
import urllib.parse
import requests
import yaml
import pika

URL = /* replace */
QUEUE_NAME = 'mean-requests'
HOST = /* replace */
PORT = /* replace */
EXCHANGE = /* replace */

TIMEOUT = 'timeout'
ENABLE = 'enable'
DISABLE = 'disable'
REGEX = 'regex'
BLKLST_CATS = 'blacklist_categories'
BLACKLIST = 'blacklist'
STATUS = 'status'
NO_TIMEOUT = 0


def publish_analyze_request(connection, message):
    channel = connection.channel()
    channel.basic_publish(exchange=EXCHANGE,
                          routing_key=QUEUE_NAME,
                          body=message)
    print('SENT [x] %r: "%r"' % (QUEUE_NAME, message))
    channel.close()

def call_gerrit_api(path):
    url = URL + '/a' + path
    content = requests.get(url, auth=(sys.argv[1], sys.argv[2])).content
    data_utf8 = str(content, 'utf-8').lstrip(")]}'")
    print('Content: ', data_utf8)
    return json.loads(data_utf8)

def get_updated_files(change_number, revision_id):
    change_info = call_gerrit_api('/changes/' + change_number + '/?o=ALL_REVISIONS&o=ALL_FILES')
    return [k for k, v
            in change_info['revisions'][revision_id]['files'].items()
            if v.get('status') not in {'D', 'R'}]
    #return list(change_info['revisions'][revision_id]['files'].keys())

def create_mean_request(project_name, host_uri, ref, paths, change_id, revision_id, config):
    source_context = {'host_uri': host_uri,
                      'project_name': project_name,
                      'ref': ref,
                      'change_id': change_id,
                      'revision_id': revision_id}
    mean_request = {'paths': paths, 'source_context': source_context}
    if config != None:
        mean_request['config'] = config
    return json.dumps(mean_request)


def mean_enabled(project_name):
    project_name_e = urllib.parse.quote(project_name, safe='')
    reponse = call_gerrit_api('/projects/' + project_name_e + '/meanenabled')
    return reponse.get("enable", False)


def get_local_config(project_name):
    project_name_e = urllib.parse.quote(project_name, safe='')
    response = call_gerrit_api('/projects/' + project_name_e + '/meananalyzers')

    mean_config = {}
    for analyzer in response:
        enable = response[analyzer].get('enable', False)
        mean_config[analyzer] = {}
        mean_config[analyzer][STATUS] = ENABLE if enable else DISABLE
        mean_config[analyzer][TIMEOUT] = max(response[analyzer].get(TIMEOUT, NO_TIMEOUT), NO_TIMEOUT) # Zero if no timeout
        mean_config[analyzer][REGEX] = response[analyzer].get(REGEX, "")
        mean_config[analyzer][BLKLST_CATS] = response[analyzer].get(BLACKLIST, [])

    return mean_config


def main():
    project_name = os.getenv('GERRIT_PROJECT')

    #if not mean_enabled(project_name):
    #    return

    change_id = os.getenv('GERRIT_CHANGE_ID')
    revision_id = os.getenv('GERRIT_PATCHSET_REVISION')
    ref = os.getenv('GERRIT_REFSPEC')
    change_number = os.getenv('GERRIT_CHANGE_NUMBER')

    config = get_local_config(project_name)

    paths = get_updated_files(change_number, revision_id)
    if not paths:
        print('No analysis started because files are either deleted or renamed')
        return

    mean_request = create_mean_request(project_name,
                                       URL,
                                       ref,
                                       paths,
                                       change_id,
                                       revision_id,
                                       config)

    if len(sys.argv) == 3:
        user = sys.argv[1]
        password = sys.argv[2]
        credentials = pika.PlainCredentials(user, password)
        ssl_options = pika.SSLOptions(context=ssl.SSLContext(ssl.PROTOCOL_TLSv1))
        parameters = pika.ConnectionParameters(host=HOST,
                                               port=PORT,
                                               ssl_options=ssl_options,
                                               credentials=credentials)
    else:
        parameters = pika.ConnectionParameters(host="localhost",
                                               port=5672)
    with pika.BlockingConnection(parameters) as publish_conn:
        publish_analyze_request(publish_conn, mean_request)

if __name__ == "__main__":
    main()
