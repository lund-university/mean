package gerrit

type CheckInput struct {
	CheckerUUID string `json:"checker_uuid,omitempty"`
	State       string `json:"state,omitempty"`
	Message     string `json:"message,omitempty"`
	//Url           string `json:"url,omitempty"`
	Started  string `json:"started,omitempty"`
	Finished string `json:"finished,omitempty"`
	//Notify        string `json:"notify,omitempty"`
	//NotifyDetails string `json:"notify_details,omitempty"`
}

type CheckerCreateInput struct {
	UUID        string `json:"uuid"`
	Name        string `json:"name"`
	Description string `json:"description,omitempty"`
	//Url           string `json:"url,omitempty"`
	Repository string   `json:"repository"`
	Status     string   `json:"status,omitempty"`
	Blocking   []string `json:"blocking,omitempty"`
	Query      string   `json:"query,omitempty"`
}

type CheckerUpdateInput struct {
	Name        string `json:"name,omitempty"`
	Description string `json:"description,omitempty"`
	//Url           string `json:"url,omitempty"`
	Repository string   `json:"repository,omitempty"`
	Status     string   `json:"status,omitempty"`
	Blocking   []string `json:"blocking,omitempty"`
	Query      string   `json:"query,omitempty"`
}
