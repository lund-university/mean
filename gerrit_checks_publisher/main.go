package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"time"

	"gitlab.com/lund-university/mean/gerrit_checks_publisher/gerrit"
	mess "gitlab.com/lund-university/mean/message"
	event "gitlab.com/lund-university/mean/message/event"

	"github.com/streadway/amqp"
)

type GerritContext struct {
	HostURI     string `json:"host_uri"`
	ChangeID    string `json:"change_id"`
	RevisionID  string `json:"revision_id"`
	ProjectName string `json:"projectName"`
}

func getGerritContext(sourceContext map[string]interface{}) GerritContext {
	return GerritContext{HostURI: sourceContext["host_uri"].(string),
		ChangeID:    sourceContext["change_id"].(string),
		RevisionID:  sourceContext["revision_id"].(string),
		ProjectName: sourceContext["project_name"].(string)}
}
func handle_POST(r amqp.Delivery) {
	var meanEvent mess.MeanEvent
	body := r.Body

	err := json.Unmarshal(body, &meanEvent)
	if err != nil {
		log.Printf("Error Unmarshalling bytes: %v", err)
	}
	handle_mean_event(&meanEvent)
}

func createCheckInput(analyzerEvent *mess.AnalyzerEvent, eventType event.AnalyzerEventType) gerrit.CheckInput {
	name := "mean:" + url.PathEscape(analyzerEvent.AnalyzerName)
	switch eventType {
	case event.Scheduled:
		return gerrit.CheckInput{CheckerUUID: name, State: "SCHEDULED"}
	case event.NotRelevant:
		return gerrit.CheckInput{CheckerUUID: name, State: "NOT_RELEVANT", Message: analyzerEvent.Info}
	case event.Started:
		currentTime := time.Now().Format("2006-01-02 15:04:05.000000000")
		return gerrit.CheckInput{CheckerUUID: name, State: "RUNNING", Started: currentTime}
	case event.Result:
		currentTime := time.Now().Format("2006-01-02 15:04:05.000000000")
		return gerrit.CheckInput{CheckerUUID: name, State: "SUCCESSFUL", Finished: currentTime}
	case event.Timeout:
		currentTime := time.Now().Format("2006-01-02 15:04:05.000000000")
		return gerrit.CheckInput{
			CheckerUUID: name,
			State:       "FAILED",
			Message:     "A timeout occured",
			Finished:    currentTime}
	case event.Error:
		currentTime := time.Now().Format("2006-01-02 15:04:05.000000000")
		return gerrit.CheckInput{
			CheckerUUID: name,
			State:       "FAILED",
			Message:     "A error occured",
			Finished:    currentTime}
	default:
		fmt.Println("This EventType should not exist for AnalyzerEvent")
		return gerrit.CheckInput{}
	}
}

func handle_mean_event(meanEvent *mess.MeanEvent) {
	switch meanEvent.EventType {
	case event.AnalyzerEvent:
		context := getGerritContext(meanEvent.AnalyzerEvent.SourceContext)
		checkInput := createCheckInput(meanEvent.AnalyzerEvent, meanEvent.AnalyzerEvent.EventType)
		sendCheckInput(checkInput, context, meanEvent.AnalyzerEvent.AnalyzerName)
	default:
		fmt.Printf("%s not relevant for checks_publisher", meanEvent.EventType)
	}
}

func registerChecker(checkerUUID, analyzerName, projectName, hostURI string) {
	checkerCreateInput := gerrit.CheckerCreateInput{UUID: checkerUUID, Name: analyzerName, Repository: projectName}
	body, error := json.Marshal(checkerCreateInput)
	if error != nil {
		log.Printf("Error Marshalling struct: %v", error)
	}
	url := "http://" + hostURI + "/a/plugins/checks/checkers/"
	POST_data(body, url)
}

func sendCheckInput(checkInput gerrit.CheckInput, context GerritContext, analyzerName string) {
	body, error := json.Marshal(checkInput)
	if error != nil {
		log.Printf("Error Marshalling struct: %v", error)
	}
	hostURI := context.HostURI
	changeID := context.ChangeID
	revisionID := context.RevisionID
	url := "http://" + hostURI + "/a/changes/" + changeID + "/revisions/" + revisionID + "/checks/"

	bytes, _ := POST_data(body, url)
	if string(bytes) == "checker "+checkInput.CheckerUUID+" not found"+"\n" {
		fmt.Println("Retry request")
		registerChecker(checkInput.CheckerUUID, analyzerName, context.ProjectName, hostURI)
		POST_data(body, url)
	}
}

func POST_data(data []byte, url string) ([]byte, error) {
	req, error := http.NewRequest("POST", url, bytes.NewReader(data))
	if error != nil {
		log.Printf("Error creating Request: %v", error)
	}
	req.SetBasicAuth("admin", "44BJ1EA7ncvw5Gykpbpse7tB3WMiMNqcmnh2VvI3kw")
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, error := client.Do(req)
	if error != nil {
		log.Printf("Error sending POST: %v", error)
	}
	return ioutil.ReadAll(resp.Body)

}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func main() {
	fmt.Println("Start Gerrit Checks-publisher Service")
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"checks_queue", // name
		false,          // durable
		false,          // delete when usused
		false,          // exclusive
		false,          // no-wait
		nil,            // arguments
	)
	failOnError(err, "Failed to declare a queue")

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	failOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			handle_POST(d)
		}
	}()

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}
